#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define int long
#ifdef TEST
char Instructions[] = "sesenwnenenewseeswwswswwnenewsewsw \
neeenesenwnwwswnenewnwwsewnenwseswesw \
seswneswswsenwwnwse \
nwnwneseeswswnenewneswwnewseswneseene \
swweswneswnenwsewnwneneseenw \
eesenwseswswnenwswnwnwsewwnwsene \
sewnenenenesenwsewnenwwwse \
wenwwweseeeweswwwnwwe \
wsweesenenewnwwnwsenewsenwwsesesenwne \
neeswseenwwswnwswswnw \
nenwswwsewswnenenewsenwsenwnesesenew \
enewnwewneswsewnwswenweswnenwsenwsw \
sweneswneswneneenwnewenewwneswswnese \
swwesenesewenwneswnwwneseswwne \
enesenwswwswneneswsenwnewswseenwsese \
wnwnesenesenenwwnenwsewesewsesesew \
nenewswnwewswnenesenwnesewesw \
eneswnwswnwsenenwnwnwwseeswneewsenese \
neswnwewnwnwseenwseesewsenwsweewe \
wseweeenwnesenwwwswnew";
#else
char Instructions[] = "wwwnewnwwnwewswnwsenwwwnwnwwwnww \
wewswseswnwwwwenwnewnwswenwsenee \
sesesesesesenewsesesesesesesesesesesese \
swnenewneneeeseeswnenweswsenenwnenene \
eneeeenenesweneseneeeenew \
neseneseewenewswnwsesenwwnenenwene \
nwwsewwwnwnwnenwwwenwwwnwwnwswse \
nwnenwnewnwsenwnwnwnwnwsenwwnwnwnwnwnwnwnw \
seseseseseeseseseseswseseseseswswsenwse \
eeweseseeeeenwneewweeseeee \
seseeseseseseseweseseneseseseseseesee \
seseseseeseseeweseseseneeseseesese \
swwnwwwnwnwwnwnwenwnwnwnwwwnwwnwnw \
eewsenwseneseseswseenwsesenwseesewsw \
wswwwswswswwswswwwwswswweswww \
nwnwnwnwwnwnwewnwnwnwseswewnwwnwnwnw \
wwswswswswwneswswswwseswwnewwswswsw \
swsenwswswswswswwsesweeswsese \
eseesenwesesewweeeneeneenewenew \
eenwswnwnwnwnwswnwnwnwenwwnwnenewsw \
wweweswnwwweswswnwwwwwswwww \
swneswwswswswswswseswswswnwswsweseswswsw \
wswseseswsesenesesesesewseswseseswnesese \
nenwnwwwnwswewnwwwnwwwswnwnwnwnwnw \
seseseswswswsenwswswne \
swswwnewneswswswswneseswsesese \
seswseswseswswsenwseswsewswseswswseesw \
wsewwwwwwwswwnewwwwwwwww \
sesesesesesenesesesewseseseseseseseswse \
enwseswswseswswswswswswswseswseswsesesw \
nenenenwneenwneenenenwnwnenenenwswnwsw \
enwseeeeeneswseewswnwnenewwe \
eewnenwwswswneneeewweeseenesene \
eswswnwnwnenwenwneswnwnwnwnenwnwnwnwne \
wwseeesenwnwneeewwseweewneesw \
seesenwneeseesweseeeeeseseseseese \
wwwswswswwswswweswswwswswwswsww \
nwnweseswswswswswswswseswseswswswswswsw \
eneseneneneeenenenenwnenenwneeneese \
senwsenenewnwseewnesesenwsesesesesew \
neeeeneneneeneeneeeeweeneesene \
swenwsweswsweseswweenwnww \
nwnwwwnwwnwwwwwwwnwwnewswwnw \
eesenweseeeeswsesese \
swseseeseesenwseseseswseswseswsesesew \
nenwnewwenenwwswseswesewnenwenwene \
wwswwwnwwnwnwnwwwwnwwneww \
seneeeeeneenwnwsweseeneeneeweee \
nenwswnwswswnwnwnwwnwwnwnenenwnwnwesenwse \
neneeswneneeeeneeeneeneneeneeswne \
wswsenenwneseswswwnwwweseneswswsene \
seseseseseseseswseseneseseswsenewsesesesw \
wswseswsesenwneswnwsewwneeswse \
swnwswwswswswwswseeswesweeneswswnw \
swswneswswswswenwswwswswseswswseswswsw \
esesewseeweswsesenwswswswneseswseswse \
nwwneswswswswswnweeenwnenwnwnenesewe \
seeseewseeseewseseeeseeeseese \
newsenwseseseswseesenenewseseseseesesww \
wneswswwswswswwswswwswswswswsewswsww \
wesesesewewseseewwseseseseeseese \
enenwnwnwnwnwnenewnwnwneswenwwnwnwne \
seeeseseeseseeeseeeesesesew \
swnwnenwnwswnenwnwenwnenwnenwnwnenenwnwnw \
nenenenenewneenwneeswswnene \
enwnwwnwnwnwswesenwnwswseeewewse \
swwnwenenewwnwseeneswenwsewenene \
seeseeweesenesenweeeeesesesee \
nwnwnwnwnwnwneswwnwnwwnenwnwwsenwnwenw \
wswswseseseswswswseneseseswsenenwswseswse \
wwwsewwwwswwwwwnwwnewwww \
wnwnwwwswewswwswwweww \
nwswswswseseeseseseseseseseseseswnesese \
eeeeweneneeneeewe \
swswswsewswswswsesenwswnwseswswseeew \
nwnwnwnwwnwswenwenwneswwnwnwnwnwnwswnwnw \
nwnwnwnwnwsenwwnwnwwnenwnwnwnwnwnwnwnwnw \
nwneswneswnwswenwneneeneseenewnwnenwse \
neewswswswseneseneseswsesewsewswsesww \
enwneneneneneneeeeneneseneneneneene \
eneeseseeeeseeeeeeeeeswee \
seeseeeseneseeeeesesesweseseswnwsee \
newnesewswneewnwneeneeneseneenenew \
ewsenwnwsweenwnesewnwseseseswsewsenw \
ewseseseseseseseseeeeeeseeseewse \
wwswswwewwnwwwwwwwwswwwnewsw \
eeswneeswnwenwenweseeneesesesenwe \
eswswswswnewwnwseswwseseneswnenenwswsw \
sesesesenwsenwneseseseneswnenwsewwsenww \
neeeeeneeeneseeneeeenweneee \
nenwwswwwwwnwwwwwwnww \
nwnenwnenwnenwnenenwnwnwnwnenwnwsenewnene \
nwwwneswwswswswsweswsewswwswswswswswsw \
nenwnwnwnwnwsenwnwsenwnwnwnw \
seeeeeeeeeeeeeweesee \
nwnwnwnwnwnwnenwnwnwsenenwnwnwnwnenwewnw \
eenesesweeeseeneenweswseeeeee \
neswsenewnenesenenenesenwneneneneneswwne \
sesesewsenwseseseseseseseseseseesesese \
seseseseesesesesesesesesesesenwseswsesese \
enwswseesewsesesewnesesesewnwseseesw \
seseswseneseseseseseseseseseseswsesesese \
nwsenwnwnwswwnwnwnwnenwsewnwnewnwnwwe \
wesenweswswneneewnenwswswswnwesew \
seseseseeseseswseseseseswswsesesenwsesw \
wnwswneeneswwnwwswenwnwswnwnenwnesenw \
swnenenwnwnwnenwnwnwswnenenesene \
neneewnesenenewnewsenwnenenwnwnenenenene \
swewwnwwnwwnwwwnewwwswwnw \
senwsewswesesenwnesesesesesesesweseswse \
seseeeenwseeseseneesewseeew \
wnweswnesenewwseswsewwnwwwnese \
eeeeseeweseseneeseseeseseneesesw \
senewesenewnwswwwnesenwewwnesw \
nwnwnwnwnwnwwnwnwnwnwnenwnwnenwe \
eeneewneesenenenenenenenenenenenene \
sweeneneneswneeneneswneneswnenenwswene \
wswwswswweswwswswwnesweswswwsw \
eeeeneeenwneneeeeewnesenenenee \
sweswswswswswswswswswswwnwswswswswswsw \
sesewseseeseenweseeseeeseseesese \
swnwswswseswseswswswseseseswswsesenw \
sesewsesesenwseseseseseseewneseesese \
swwswswswswswswswwswwwswswneswswswsw \
eenwseeeeenwneeenwsewswseeswee \
swswseseseseseseswseneswswswswseswswsesenw \
wwswswnwewwwswswesw \
neneneneenwswnenenwwnenwnenw \
seswseswswsweswswswwnwswseswseswswswsese \
seseseeweseenweseeseesweeneeese \
swswwwwwwswwwswwwswwnenwewww \
swwwewnewwwwwenewswwwww \
eeenenesenwseeneeeew \
wwwwnwswswswwwewwwwwwswwswe \
seenwseseseseseeneseseeseswseseswsesese \
sesewnwesenwnewseswwneneseswseswew \
swswswswsweseswswseswswswseseseswswsew \
eneenwneenwseeseneeeenwswweswsww \
seewneseseseesesesenesenwsesewseeee \
sweeesewneweweeeese \
nwnwnwnenwnwnwnwnwnenwnwnwnwsenwnwnwnew \
wwswswneneswswswswseswswswswswswewswnwsw \
wwnwwwwswwnewnewwwsewwwwwnw \
sewsesesenweseswsesewswseseneeswsese \
seswswswswswswswswswswneswswswswswwsesesw \
eeseenesenwsewseneeesenweswnweesw \
swseesenwewneseseeseswswnewewnesw \
nenesweeeenenewnwneeeneneswnewsene \
swwseswsweswseswsesesenwseeswswseswsw \
nwnwneswweswesenwnwnwwnwnwnwnenenwwwse \
wwwwswwwwnwswwwwwwwewww \
eseseneeseswwneweeswswnenwnwneswese \
senwwnwnwenenenwsenew \
swswswswseswswswneswswswswswswswswseswnw \
seswsesenwswseseswseswswswseseswswswswsw \
enweenenweesewneeeeeseeswseenenw \
neneneneeseweeneneneeeneneneeene \
esesenesenwseswseseswseseneseseseseww \
eswswsenewseswseswenweswwnwseseswsenw \
seseneseseseseseseseseseseewseesesese \
nenenenenenenesenenewnenene \
swswswswwswswswneswenewswswwswseswwsesw \
nwnwwenenwsenwnenwwnenwsenenw \
seeneseseesewsewnweneesesewesese \
neseeseenweneeeneneweesweeenww \
swsesenwseseseseseswsesesesesesesesesese \
swwseneseswsewseseseneswseswswseswseeswse \
nwnwneewnenwnwnwwwnwnwwnwswwswsese \
wwewwwsweswwwnww \
swnwnwnwnwnwwnwewnwnwneenwnwnwwswnw \
swwwwswwwseneswswwswwwswwswwww \
neeseseseseewsesewseenesesesesenenwwse \
nwnwwnwnwnwnwnwnwnwnwnwnwsenwwnwnwnwnw \
eeweneeeeeeeeeneseeeeee \
enwwesweeeneeneneeeseneenenenew \
swswsenwswwswweswwswwswswwwwswne \
wewnwwnewnwwnwnwnwwnwnwwnwswwswnw \
nenwneneneseneeneneeneeneeenenenene \
nwwswnewsewnwwwwwnewwww \
eneneneesenwneneeneeeswswnesweenwne \
wwwwwwwwwwwnwwwwsenwwenww \
swswnwesewwswswwwwswswwwwnewswwsw \
wwnwwwwwwnwnwnwwwenwwnwww \
seesenwseseseseseswswseswseswwnwseswsw \
nwnwnwnwnwneneneneswswnenwnenwenwnwnenenw \
sewwwsewnewnwnwsenwwnenwnwswnwnwnew \
neneseeneeeneneneneeneenwneneneee \
sweswewswwswnwnwswswswswsesweswnwwsw \
wwnenwenwsenwnwwnwnwwnwnwnwnwwwnw \
eneeneweeesewneneeeeneneeeee \
swswnewswswswseseseswswswswneswseswswsw \
wwnewwswnwewsw \
neenwneeeswenweneseneeeneenenese \
seswswnwsesesenwswseswseswseeswswswseswse \
neneswswswswseswswwswneswswwswsweseneswne \
sesewnwwnwnenwnwnwenwnwnewneenwnenw \
wswsenwewnewewwswswwswswwswswwwne \
senwnwsesesweswswnesewseseseeesee \
eeswneneneeneenenenenenenenenenenene \
swwseswswswswswswswneswswseseswnewswswswne \
eswseswswswswswwswseswswseswneswswnesw \
swsesenwwnwswsenwseseeswsesenenenwwse \
seesewseseseeeseseseseeeeseeese \
swseseswswneswseswswswseseseseswswswsesw \
wwwnwwwewnwnwwwwwwwwwswww \
nwnenwnenenenenenenenwneneswnenenenwswene \
neseneneneseenwnenenenewnewnwnwnenwnww \
wwnwweenwwwwwwnwnwwwswwwwnw \
wnwwsenwsenewnewnwwnwnwwnwwnwww \
wnwwwwnwnwwwwwnwnwwswwnwweww \
wswswwwswswwswwwneswwseweswwe \
seeneseeeseseweeee \
senwnwnwenwnwnwswwnwnwenwnw \
eneneeneenwneeswnenenenenewnenenee \
swwweeseseeseeeneneseseseseeeene \
seseeseseeseeseseseseneseseseseswese \
neswsenwswsesesesesenwseseseseeseesesw \
senwswnenwnenenenwewnwnwnenwnwnwne \
nweenwswseswnwneseneseseswnewseswnwwne \
swwwsewwwewwswwwwnwswwwwswnwsw \
wnwnwnwenenenenwnwsenwneenwneseswnwswnwnw \
neswnenenwneneenenenenwneseweswnenwsene \
wwswneneswnwsenewneeneeeenenewsw \
eseneeeseeeseseseswseseneewseewe \
nwnenwnenwnwnwnwwnwsenenenwsenwnwneswne \
wwwsewwwenewwsw \
nwnwnenwnesenwwwwwnwnwwnwsewewnw \
nwnwenwnwnwnwnwnwnwnwsewnwnwswnwnwnwnww \
swnenwnewwnweswswenwnwnwsenwwnwenw \
nwnwnwnwnwwenwnwwewwnw \
swswnwnewnenenwseswnenenenenenenenwswesene \
swswsweeswsewseswsesesesesesewswswsenw \
wnwwswswwwswsewswneewswswswwneww \
eweeeeweneenee \
seeswseneneseneswsewneswsewswswwsese \
nenesenwneneneneewneeneeee \
wswswwsenewswswewnwwwswsw \
wneenenwnwnenwnesenwnwnenwnwnwnwnwwnw \
enwseewenwswswnwnwseswswnweswsesenwnw \
nwswswsesenenwnwwnenesenwnew \
neswwsewwneewswswnewswwnwnwwsesw \
nwnenenenwneneeswnenenwnenwneswnwnwnene \
neesewnwnwwwwnw \
seeseseseweseneeeseesenewseseese \
sweneneneneswsweeeesenwnenww \
neneneneneneneseneneneneneewenene \
seseseenweseseseeseseseseseenwse \
seswswswswwwswwwswswswneswswswwnwswsw \
swswwswswswnwswsweswswewswswswswweswsw \
senenenewnwnenwsene \
swnenwnwneswnwnwnenweenwnw \
sewwwseseesenesesenwwswseesesenesese \
nwnwsenenwnwnwnwwnenwnwnenwnesenwsenenenw \
eeeseseeeseseeseeeenweseseee \
neseseseswnwsenwsenwsesesenenwsewsesese \
eeeeeneeseseseseesewesweeneesese \
nwwwwswnewwwwwewenenesesewsenw \
wnenenwnwnenesenwnwnenwswnwnenwneeswnwnw \
seeswswnwseenesenwsesesenwwenwswwswse \
seseseneseseseswseseseneseneseseseswwse \
nwnwnwnwnweswenwnwnwnwswnwnwseswewnwse \
eeswneeeneeeweeneeenweenenew \
swneswswwwnwswwwswnewswseswwewswne \
sesenwseseseseseseesesenwsesesesesese \
enwseneswwswewwswwewwwwswnenwnww \
wwnewwwwwwnwwwwnwsewwwnwwnw \
nwswwwswwswwseswswswwwswwwswww \
eewnwsweeeeeenweeeenesweseee \
seeeseeeeeeseseeseseseseeesenw \
neseswsenewseseeswswsewne \
nwseseseseseseseswneesewseswswseswsese \
swnwnwnwwnesesenww \
senwnwwnwneweswnwwsenesewwnewwne \
eenwsweeeseeeseseeeeseeseesee \
eeseswneseeseeeswesenwseseeseese \
eeeseseneesesweeseenweseeesewew \
wwwswnweswwswswswswnwswnewseswswsw \
nwwswnewseswswswnewwswewwwswseww \
swnwswswwsesweswswswswswswswswswswswswse \
nwswwswneswnwwweswwwseswwwwswwwsw \
ewwnwswwnwnewseww \
wwwwwsesewwwwnenwewwnwwseww \
seseseesenweseseeseseenwsesenwseswnwswnw \
swswsewswswwswnew \
neneneneneneneneneswnenenenenenenenenene \
wnwswwewwnwsewweneswwwwswweww \
seseseseesewswseseseseenenesesesesese \
ewswswwswwswwewswwwwnwwsweww \
nwnwnwnwwsewnwnwnwnwnenwnwnwnwnwsenewnwnw \
seseswswswswnweswsenesesesesenwsesese \
nenenwnwwnesenesenwswnenwnwnweewnese \
wwwswswswwwewwswwwwwwwswsw \
nesewweenwswnesewsewnewnwenwsewse \
swnwwwnwwewwewseswwswwswwsww \
eseeeeseseeswneseeeesenwse \
swswswsesweswswswswswswswswswnwswswnwswswsw \
newwneswwwwsewwwswsewwwseneesw \
senwswseneeswseseesenwwnesesesweseesese \
nwenewneenesenenwneswnwnwsenwnwnenwwnw \
eeeeneesweeeenwneweeeneeee \
wewwwwwwsewwwwswwswwwneww \
nenenwneswewswneneswsenesenew \
nwneswnwnwnwneenenenwsenwnwne \
nwseweenwnweweeeeswsesenwwee \
enwseswwwswwwwnewwwswswswwswswsew \
nenewneeneneenenesenenenesenenewnene \
seswsweeseswswsewseswswsewnwswsw \
nwnwnwnwnwnwneenwnenwnwnwnwnwnwwnwnwnw \
eeeneeeeeeswneenwneeeeeswe \
senwnenwnwnwnwnwnwnwwnwnenwneenwwnenwse \
swwwnwnwwwswwnenwwnwneenweseseew \
sesenesewsewneswsesesesenwwesenwsese \
newwnwneewsewwswwnwnwwwnwwswnw \
nenwwswwnwwwwnw \
nenenenwneswneeeenwenenesweneswnenene \
nwwswwswwwnesewwwwswwswse \
sesenwseswswswseswswswswsw \
eesweeeeneee \
swewswswsesesweswswswseseseswswswsenw \
swswswseseswsenwseseseswsweswswseseswsesw \
wwnwenwnwwwsewwewwnwwnwwww \
swswswwswswswwswswneswswswwseswswswsw \
wewseseenesenwsenwnenwnwnwnwsenwnwnwnw \
neneswnenwnenenenenenwneswnenwne \
seswweswswseseseswseswswsweseswswswswswnw \
swswswwswswwwswwwneswswseswswswswsw \
eneneeweeseeeneneewe \
nwnwnewseneneeneneseswswnenenenenwnwnenw \
wneeeneneneneseneneeswnewneeweese \
nwsenwnewnenwnwnwnenwsenenwnesenwnewnwnwnw \
eesweeseeeeenweeeeeweeee \
nwnwnwwwnwwwnwnewwse \
wseseenwnwsenwnwnwnwnenenwnwnwnwswnwnwnw \
wnwsenwwnwsewnwnewnwnwwwwwneww \
nwsenwnenesenenenwnenenenwnwwnwwnenenenwse \
neeseseenesewsewnwnesweeewwneeee \
swseseseseseseseeeseseseseesesesesene \
eweeeseeneeseeseeseweseesesw \
wwnwsenwneswnenenwnenweseneneswnwswne \
neewwnesenesenewenenenenenenenenenene \
nwnenwnwnwenwnwwnwwswnwnenwnwswswsew \
esenenenenenenenenenenenewneneneneseewne \
senwseswseswswseeseswseseseseseswsesesese \
wnwewwnwwnewsewnwww \
seewwseseseseswswseseeseswswseseseswnw \
seneseeswnwsesesesesenweesesenwwsee \
wwwswwwwweswswww \
sesesesewnwsenwsweseweseseeseseenesese \
swwwseseseswnwwnenwnwswsw \
swseseseesenwswnewsenwseswseseseswnesesw \
swswswwwswewswwnwswwwwwswswswsw \
neneswseswswnwneswnwswswswswsewswsesw \
eeeewenenweeeeesenwseeeew \
swseswseswswnwswswseswsweswswswwswswsw \
nenenewnwnenenwnenwnenwnenenesenenenenenw \
wwwwnewnwwsewwnwwwwwsewnewwnw \
nwswswseswswswsenwseswswseswswswswnwsene \
nenenwsweneneneeneneewneneewseswnenwne \
seseswswseswseswswneswsweswnwenwseswswsw \
nwneswnewnwnwseewewneeneswnwnwenesw \
senwnwnwnwnwnwnwwswenwnwnewnenwwnwsee \
nenenenenenenenwnenenenenenenesenesenewnw \
eenenwenweeneneswneneesw \
newwwwswnewwnwwwsewwwsewwse \
enwnwnesenenwenweswsewseweswnewswse \
nwnwnwnwnwnenwnenenwnwnenwnwnwswnwswnenwenw \
nenenweeneneneeeneeeneneseenene \
eswwsweswsweswswswwswswwsw \
eeneneenenenenwneeneneneneeneeneesw \
wwwswnwwewwnwnwnwnwnwnwenwnwwnw \
swswnwswswswwwswswneswseeswswswswswww \
eeneneseneneneneneswnwneeseneenenwne \
nwwwseenwnwnwwsww \
nwesesenwnwsesweeseseseeseeseesee \
weweeeweeneeseeeswseeenwew \
wswwwwwwwwsewwewwswwnenew \
wewneeeeeesweeeeewswenene \
nwwneewsesewswwwnwewwsewnwnwnw \
seeseswenwneseeeeeseneseewnwwwnw \
neneneswnenenenwnenenenenenenenenenenene \
sewnwnwnwwwswnwewnwneswwnesenww \
neswsesewswwsenwswnwnweswne \
eneeneswneeneeweeeene \
swnwsenwnwnwnwnwnwnwnwnwnwnwewnwnwnwnw \
eseseseseseseeeesenwseseeseseewse \
nwnwnwnwsenwwwnwwwnwnwnenwswnwnesenw \
neswneswneseneneneswenenenenenenenewnenw \
seenwenweeseswesweeeeeeeese \
wnenweswwnwneneneenenenwnwesewnenw \
neneneeswenenenenenenenenenwnenenenenene \
weneeswnweeweeeseseeeese \
wwwwwnewwwswnwwwwwwwwww \
nwsenwsesesenwwwnwnwwewnwnwsenwnwnenw \
nwnenwwsenenwesenwnwwsewnwnwnenewsw \
nwnwnwnenwnwnenwswnwneneneenwsenenwnwnew \
neneneneneneneneneneneneneseewnenenenwne \
nenewnenwneneeseeewseenenwnenesesw \
enewswswswswswswweswswwseswwswswenww \
wswswswswswswswswswsweswswneneswswswne \
wnwnwnwnwenwswnwwnwwwwnwnwwenwnwnw \
seseseewesesenwseseeeseese \
nwnesenwnenwnwnenwnwenenwnenwneswnwnenwsw \
wesenwneswwswwswnewwswwwewww \
sewewnwwewsewnenenwseswwwswweswsw \
eswswswseswswwewwwwnenwwnewsww \
wnwswwswnwneseswseswswsesesweeesesenene \
seseswneneseswseseswnewseseseseswsesew \
wwwnwwwwwwwneewnwwwsew \
esenewsewseswnese \
wnwnwswnwwnewnwwnwnwwnwnwnwwnwww \
sewseseswseswneeswseseesesesewsese \
wwwswewnwwwwenwnwwnwew \
neneseeneeneneneeeneswwnwnwneneeenene \
eeeeeneeeeeeeeesweeeene \
wwwwswwwwwnwwwsewwnewwwwne \
swsenwswsewneswswswswswswseneswswsw \
nenewweneeseenesenwnenwwneeeseswsene \
swswwwswswswwneswswswweswswwswwsw \
sweneewwswwswswswneswswwnwwswwswse \
wseneneswneswnenenenenenwenwnwswswsenenee \
wswwswwwneswwswseswswswwwswswene \
enweeeeeeeeeeeeeeeswee \
enwseseeeeneeewenweeeesewe \
nenwnwnenenwnwneenenwnwnwnenenwswnwnene \
nenenewneswneeesenwneneneneewenenene \
nenwneneneneneneneneseswnewneseenenenenew \
nwnwnwnenwnwnwnwnwswnwnenwnwnwnwnwenwnenw \
seeswnwseseseswseswseseswswswswswswsesw \
nwewewnwnwnwewnwwsewnwwswweww \
nwnwnwnwenwnwnwnwnwswnwnwnwnwnwnwnwnwnw \
seseseneswwsweesesewseseseswseswwne \
wseneseseeswswswsewseswswseswswwese \
wwnwswwwnwnwsewwwenwnwnwww \
wseswseseseeseseseseseseseseswsesesese \
swsesenwswwswsweswneswswnwswswswswsesw \
weeeenenenwseewnesweneeeneswnene \
eenwnenenenenenenwneesenenesenenwnwsesw \
swesweswnwnewenwnwnwwnwwenwwwnwnw \
seewsenwswswswseseseseseseseswseswswsese \
neeenewsewneneneseweeneneenenwne \
nenwnwnwnwneeenwnenwnenenwswnenenwwne \
swswwswswewswswwswswswswswswswswswsw \
eseseseseseseesewenwseswseeesesese \
nwnwnwsenwnenwnwwnwnwnwnwnwnenwnenene \
esweenwneeeeseenw \
sesenwseseesesesesesesesesesesesesesenw \
wwwwnwwnewwwwwwwnwwse \
swnwewnwneeswweneeeseeenenenenene \
newenewnenesewswnenwsenenwsenwenenw \
seneseseseseseeswseseseswseswnwswseseswse \
nwwwwwwwwwwswwwnewwwnwwnw \
eenwsweewswnwewnwseswswswwwsesw \
eeneneneeneenenesweneneeeeenwne \
nenewnenewneneneneneneseenenwnenwnenene \
weeenweneeneeeneneeneeeeesee \
nenwneneeesenwswsewnewwnesweenesene \
wswnewneseswsenwsesewnwswsenenesewsw \
swswswnwnwwswsewnenewnesesww \
nwwenwnwwnwnwwnwseneenwwnwsew \
seeseseseseswseseseseseseseswsesenwswnwnw \
sewwwwswwewwwwswwwswwneswsewne \
seneseneneenenesenwnenesenwnenewwenene \
nenenenenenenwnesenenenenw \
swwswswswswweweswwswswswwwwwenw \
wwwsenwswswswwewwwswwwswwneww \
wwwwwsewsewwswnwwnwwnwsewwww \
eeneswswnwewweeneesweee \
eneswswwswswswswswswwnwswwswwwswse \
swseseseseswnwseseseseneswswswswsesesesw \
swseseseseswsewswseseswsesenesesesesese \
neeeweeneeenesweeneeewesenene \
nwswswswseswswswseswsenwseseswseseswswsw \
swneeseneneneenwenweeeeewnenene \
wwsesweswnwwwwwwenwwsewnwnwne \
swswseseswswsewswseeswsese \
wwwwswswswwwswswnwewwswswwwsw \
swwsweswwswewewwwswnwewwww \
esenwsewsenesweseesenweesee \
eneeeeseeseeeneswweneseenewwe \
swnwswswswswswswswswswswswswswswswswswesw \
eneeseeesesesenwwenwsese \
neeneneeesweeneeneneeeneeenenenw \
wsenwwwswwwwnwnewwnwnwwwnwwww \
neseeneneneeneneneneneenweneeeee \
nwneswwswwswnwenwwneneseeseseswwswnw \
nwnwnwnenwnwnwewnwwsenwnwnwnwnwnwnwnwne \
ewwnewseenwwnwew \
nwnenenenenewnenwnenwneneenenwnenenwnw \
nenwnwnewnwnwnwwsewsew \
eseeseeeeeeseeeeew \
seseeseeseseswnwsenwsesenwsesesesenwsenwse \
wnwenwnwenwnwsewnwnewnwneswwewswsw \
neneneneneneswnenenewnenwnenenenwnesene \
sesesesesesesenweswseseseseseesesesesese \
enweeeeeeseseeeeenweeeeese \
sesesesenewseseneseseneseseseswsesesewse \
seswswseweswnwswnweseswneswwswswswse \
wsenwwnwwwwww \
nwsewwnwwswwnewnewwwwwswww \
newwwsewwnwnewwwswswwwswwwsw \
sesenwneneswseswesewseseeneseeswnwnw \
nwseseseeseswseseeeseseswsenwesenese \
nwnwswswswewswewsewswwsw \
sesenwseseseseneseseeswseneseswseswwsese \
ewwwsewwwwewwnwwwnwwwww \
eeswneenweeenwseeeesweenweee \
wswseesweswswnwswwwswewswneswswsw \
swseseseswsweswwseeseseswswnwsenweswse \
seseswwseseswseswswseseseswnesesesesesesw \
swneswswnewenwnesewnesewnenenwseeene \
nwenwnwnwnwwnwnwnwswnwnwsenwnwnwnwsenw \
wnwenwneswseesee \
eeeenweseseeesweeeeeeenwnw \
swswswswswswsesweswswswswnwswswswswswswsw \
nwwewwwwwwewwwwwew \
nenenwnwswnwnwnwnwnwnenenenwnwnwenwnenwnw \
neewnenewnwnenenwsenwe \
sesewsenwseseenwnwswene \
nenwnwenenesenenewneswnenenenesenw \
eeeseeesenweeenwenweeeneese \
wnewswnwwwwwswwswseswswnewwwnew \
sesesesesewseseesenesesesw \
nenenesenenenenenenwneenene \
swsesesesenesewseseseseswsesesesesesene \
neeneeeneeneneenenweeeesee \
senwnwnwnwenenenwnwnwswnw \
swswneswswneswswwwswwswswswswswwsww \
wwwwwwsewwwwwwwwwnewnwsew \
eeseesesewneeeseseseseeeeesesese \
neswnenwswswwswsenesweswwswswsweswswsw \
swseswwwwnwswsenwwwwewwwnesww \
neeseeswnwnwsweswe \
wenwnwnewnwnwsenwnenenwnwsenwnwswnwnwne \
eweswneeesesesenwneeseeesesesewsee \
nwneswneenwnwnenwnwnwnenwnwnwnwnwnwnenw \
swseseseswswneswnwwseseseseswsesesesesese \
nenwnewenenwneswsenenenenwnenewsenenenw \
nwnwnenwnwnwnwwnwnwnwnenwnwse \
esesewsenweseseseeneseee \
swsesenweeeeeenwneeeeesweeswe \
nwnwnwwswewsewwwwnwsewwwnwwe \
nwnwenwnwwnwnwnwnwnwwnwnwnwnwwwwwe \
nwwesweewsesenwsenewnwswnwsenwswsesw \
nesenenwnenenenenenwswnenenwnwnenwnew \
seneewnwnesweseneenwnenewnenenene \
nenenenwneswnwnenenwnwwenwenenenwnwnenw \
nwnwenwnwnwnwnwnwnwnwnwnwnwnwnwswnwww \
nenenenenenenenenewneswneneneenenene \
swnwenwnwnwswnenwnenewwweseeseewswne \
eeenweeneesweeeeeeeneeee \
eeneeneweneee \
wseswenewseeneenwneneseenwwesesw \
nwnwnwnwnwenwwnwnwnwwwnwwnwnwnwwnw \
nesewswswsesewesweswswseseswswnwnene \
nwswswnwwnewwwsesesenwesewswnese \
wwwsewnenwsewwswswswwnwwsewewsw \
eswsesewseseneswwseesenenwnwnweswseswse \
swswwswswseswswswswswswswswswswswswneswsw \
nwneseseswwnesewsenwswnenewsene \
wswswswnwswswswswswswswswsweswswseswnesw \
senesenesewseeewseseswsesesesenesesese";
#endif

#define HASH_COUNT 1024

struct hash_entry_struct;
typedef struct hash_entry_struct hash_entry;

typedef struct {
	int X, Y;
} coord;

typedef struct hash_entry_struct {
	coord C;
	int Flipped[2];
	int Neighbors[2];
	hash_entry *Next;

	hash_entry *ListNext;
} hash_entry;

typedef struct {
	hash_entry *Entries[HASH_COUNT];
	hash_entry *First;
} hash_table;

int Hash(coord C)
{
	unsigned Result = (unsigned) (19 * C.X + C.Y);
	return  Result % HASH_COUNT;
}

void Flip(hash_table *Table, coord C, int Generation)
{
	unsigned H = (unsigned) Hash(C);

	hash_entry **Current;

	for (Current = Table->Entries + H;
	     *Current != NULL;
	     Current = &(*Current)->Next)
	{
		if ((*Current)->C.X == C.X &&
		    (*Current)->C.Y == C.Y)
		{
			(*Current)->Flipped[Generation] = !(*Current)->Flipped[Generation];
			break;
		}

	}

	if (*Current == NULL)
	{
		*Current = malloc(sizeof(hash_entry));
		(*Current)->C = C;
		(*Current)->Neighbors[Generation] = 0;
		(*Current)->Flipped[Generation] = 1;
		(*Current)->Next = NULL;
		(*Current)->ListNext = Table->First;
		Table->First = (*Current);
	}
}

enum token
{
	E, W, SE, SW, NE, NW, END, COMPLETE_END, NUM_TOKENS
};

int Stride[NUM_TOKENS] = {
	[E] = 1, [W] = 1,
	[SE] = 2, [SW] = 2,
	[NE] = 2, [NW] = 2,
	[END] = 1, [COMPLETE_END] = 1
};

int CountFlips(hash_table *Table, int Generation)
{
	int Flips = 0;
	for (int Hash = 0;
	     Hash < HASH_COUNT;
	     ++Hash)
	{
		for (hash_entry *Current = Table->Entries[Hash];
		     Current != NULL;
		     Current = Current->Next)
		{
			if (Current->Flipped[Generation]) ++Flips;

		}
	}

	return Flips;
}

enum token NextToken(char *String)
{
	enum token Token;
	char c = *String;
        switch(c)
	{
	case 'e': Token = E; break;
	case 'w': Token = W; break;
	case 's': Token = NextToken(String+1) + 2; break;
	case 'n': Token = NextToken(String+1) + 4; break;
	case ' ': Token = END; break;
	case '\0': Token = COMPLETE_END; break;
	}

	return Token;
}

void AddNeighbor(hash_table *Table, coord C, int Generation)
{
//	printf("Adding neighbor to %d %d\n", C.X, C.Y);
	unsigned H = (unsigned) Hash(C);

	hash_entry **Current;

	for (Current = Table->Entries + H;
	     *Current != NULL;
	     Current = &(*Current)->Next)
	{
		if ((*Current)->C.X == C.X &&
		    (*Current)->C.Y == C.Y)
		{
			(*Current)->Neighbors[Generation] += 1;
			break;
		}
	}

	if (*Current == NULL)
	{
		*Current = malloc(sizeof(hash_entry));
		(*Current)->C = C;
		(*Current)->Neighbors[Generation] = 1;
		(*Current)->Flipped[Generation] = 0;
		(*Current)->Next = NULL;
		(*Current)->ListNext = Table->First;
		Table->First = (*Current);
	}
}

void AddNeighbors(hash_table *Table, coord C, int Generation)
{
//	printf("%d %d is neighboring:\n", C.X, C.Y);
	coord Neighbor;
	Neighbor.X = C.X + 1;
	Neighbor.Y = C.Y;
	AddNeighbor(Table, Neighbor, Generation);
	Neighbor.X = C.X - 1;
	Neighbor.Y = C.Y;
	AddNeighbor(Table, Neighbor, Generation);
	Neighbor.X = C.X;
	Neighbor.Y = C.Y + 1;
	AddNeighbor(Table, Neighbor, Generation);
	Neighbor.X = C.X;
	Neighbor.Y = C.Y - 1;
	AddNeighbor(Table, Neighbor, Generation);
	Neighbor.X = C.X + 1;
	Neighbor.Y = C.Y - 1;
	AddNeighbor(Table, Neighbor, Generation);
	Neighbor.X = C.X - 1;
	Neighbor.Y = C.Y + 1;
	AddNeighbor(Table, Neighbor, Generation);
}

void NextGeneration(hash_table *Table, int CurGen)
{
	int Next = !CurGen;

	for (hash_entry *Current = Table->First;
	     Current != NULL;
	     Current = Current->ListNext)
	{
		if (Current->Flipped[CurGen]) AddNeighbors(Table, Current->C, CurGen);
	}

	for (hash_entry *Current = Table->First;
	     Current != NULL;
	     Current = Current->ListNext)
	{
		if (Current->Flipped[CurGen])
		{
			if ((Current->Neighbors[CurGen] == 0) || Current->Neighbors[CurGen] > 2) {
				Current->Flipped[Next] = 0;
			} else {
				Current->Flipped[Next] = 1;
			}
		} else {
			if (Current->Neighbors[CurGen] == 2) {
				Current->Flipped[Next] = 1;
			} else {
				Current->Flipped[Next] = 0;
			}
		}
		Current->Neighbors[Next] = 0;
	}

}

int main(int argc, char *argv[], char *envp[])
{

	hash_table Table;
	memset(&Table, 0, sizeof(Table));

	enum token Token;
	char *String = Instructions;
	coord Current = {0, 0};
        int Generation = 0;
        do {
		Token = NextToken(String);
		String += Stride[Token];
		switch(Token) {
		case E:  Current.X += 1; break;
		case W:  Current.X -= 1; break;
		case NE: Current.Y += 1; break;
		case SW: Current.Y -= 1; break;
		case SE: Current.Y -= 1; Current.X += 1; break;
		case NW: Current.Y += 1; Current.X -= 1; break;
		case END: /* FALL THROUGH */
		case COMPLETE_END: Flip(&Table, Current, Generation); Current.X = 0; Current.Y = 0; break;
		};
	} while (Token != COMPLETE_END);

#ifdef PART1
	int Flips = CountFlips(&Table, Generation);
	printf("Flips: %d\n", Flips);
#else

	for (int Day = 0;
	     Day <= 100;
	     ++Day, Generation = !Generation)
	{
		int Flips = CountFlips(&Table, Generation);
		printf("Day %d: %d\n", Day, Flips);
		NextGeneration(&Table, Generation);
	}
#endif

	return 0;
}
