chars(X) :-
    word(Y),
    string_chars(Y, X).

match_char(Char, Current, []) :-
    end(Current, Char).

match_char(Char, Current, Follows) :-
    rule(Current, [First | Rest]),
    match_char(Char, First, FFollows),
    append(FFollows, Rest, Follows).


match([], []).
match([First | Rest], [Current | OFollows]) :-
    match_char(First, Current, Follows),
    append(Follows, OFollows, MFollows),
    match(Rest, MFollows).

run(X) :- chars(X), match(X, [0]).
main :- aggregate_all(count, run(X), Count),
	print(Count).
