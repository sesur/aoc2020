(ns random-stuff.day-20
  (:require
    [clojure.string :as string]
    [clojure.set :as set]))

;; 2729 1951

(def test-input "Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...")

(def raw-input "Tile 1913:
##..#....#
.#.#.#.###
#...##.##.
.....#.#.#
.#......##
.........#
#...###..#
.#..#.....
.##...#..#
#.#...#...

Tile 2579:
##...####.
##..#....#
...#...#.#
#.....###.
##..#...##
#....#..#.
.#.....###
.##...#...
#.........
...##.....

Tile 1531:
.......###
.#....####
.#.#.#...#
....#..#.#
.#.#.##..#
.#.#.#...#
#.##...###
##..#.....
#...#.....
##.##.#..#

Tile 1493:
#.##.#.#..
#.#..#.##.
..#.....#.
#.#.#....#
....#....#
...#####..
#####.#.#.
#.##..##..
..#.....##
.##..#.###

Tile 3109:
########.#
......#.##
#....#...#
##.#.##...
....#.#...
#####...##
....#....#
..##...###
.......#.#
#.##.#....

Tile 3343:
###......#
#..#.#..##
...#....#.
#.........
..#.....##
.....#####
.....####.
##.......#
..#.......
#.......##

Tile 2609:
####..#.##
#.#..#....
....###..#
###..#..#.
#.#......#
##..#..#..
#....#..#.
#......###
#..#....##
.#..#.###.

Tile 1823:
###..#....
..###...#.
..#.###...
...#.....#
#....#.#.#
.#..#.#..#
#..##.#..#
...##.....
....#.#.#.
###.##...#

Tile 3391:
....#...#.
.#.#....#.
#......#..
#..#....#.
#.##...#.#
...##.####
....##..#.
##.#.#..##
.....#..##
#.#.#..#.#

Tile 2753:
....###..#
.#..##.#.#
...#......
..#.###.##
...#..##..
....#.....
#......#..
#........#
......#..#
#..#.#...#

Tile 3989:
.#.###.#..
#......##.
.##......#
.......###
###....###
##........
##....#.#.
...#.#..#.
........#.
###..#..#.

Tile 1877:
...###...#
#.#..##...
.##.###.#.
#.#....#..
#..#.#.##.
#.......##
####..#.#.
##.#....##
.##.......
.#..##..##

Tile 2729:
#...####.#
...#.....#
.###......
...#..#..#
...#.#.#.#
.#.....##.
..#.....##
........#.
###.......
#..##...##

Tile 1031:
##.#..####
#.##.#....
..#.......
#......###
.##.###.##
#...#...#.
#..##.....
#..#.#.#..
....###...
....#.###.

Tile 3803:
#..#..##..
...##..#..
...#.#....
#.....#..#
#........#
.#..#.#.##
..#..#.#..
#..#.#.#..
..##..#.#.
#####.###.

Tile 3541:
.##.#..#..
.##..#...#
.#.#..#...
...#..##.#
..#.....#.
..##......
.##......#
##.......#
.#..#.##.#
###.#.##.#

Tile 3821:
.#.#.#....
..#..##...
#.##......
..#...#.##
#.####..#.
##.##.#...
..#..#....
....#....#
...###.#.#
.#..#.#.##

Tile 3853:
#####..#.#
.....#..##
#...#.##.#
.##....#.#
#.#......#
###...#.#.
##....#.##
#.#.#.#..#
....###...
##.#.#....

Tile 1249:
###...####
..#....#..
##.##....#
.........#
#.........
.##.#.##..
...#...##.
..........
##...#.#..
##.####..#

Tile 3673:
...##..#.#
.#..#..#.#
###......#
#.....###.
..#.#.....
#.#..#.###
.#.##.##..
.##....#..
.#...##..#
#.###..#.#

Tile 3163:
##.#.#####
.##....#..
####......
#..#.#..##
.#.#..##.#
##.....#.#
.......#..
#...#.....
..#..#....
###.#...#.

Tile 1759:
###.#...##
...#..#...
###..###..
.#.#...#.#
.##...#...
..###....#
#.#.....#.
......###.
..#.####.#
##..##..#.

Tile 1831:
.#...#...#
...#.#..##
#...#.#...
#...##.#..
.#.#.#..#.
......##.#
.........#
#....##.#.
......#..#
#..#...##.

Tile 2179:
.###.....#
..######.#
....#....#
...#....#.
#.##..#...
#..#....##
.#..#.#..#
..#.#....#
#.#.#.....
###.##...#

Tile 2713:
..###.#..#
#.#.....#.
#....#.#..
#...##....
##...#.#.#
###..###.#
.##..#..##
.#...##.#.
#.#.......
#...#.....

Tile 1889:
....#....#
......#.#.
##.###...#
.#..##...#
##.##.#..#
...#...##.
#....#...#
.......#..
.#...#..##
.#.####.##

Tile 1999:
##....##.#
#..#.##.##
..#...#...
#.....#...
......#...
#.....#..#
.#.##..#.#
..#..##..#
#...#..#..
###..##..#

Tile 2039:
.#....####
.##..#....
.#.......#
#........#
#.......#.
.#..#.....
#.##......
##....#..#
.........#
#.#......#

Tile 2969:
...#####..
..#.#...##
##.....#..
##.....###
#...#..###
#..#....##
#..##...##
.........#
#...##...#
.#......##

Tile 2153:
.##..###..
#.#..#..##
.#####....
#.#..#.###
#.....#.##
####..#.#.
..........
.#......#.
...##...#.
.#....####

Tile 2837:
..###.....
###.#.###.
.#....#...
##.....###
##.##..###
..#..##.##
.#....##..
#..##.##..
#..#......
#..##...#.

Tile 1619:
##..###.##
##....####
.#..##.#..
##.#...#..
#..##.##..
...#.#.#.#
.....#.##.
..##..#..#
#..#..#..#
......#.#.

Tile 3943:
.#.##.#.#.
.......#.#
.........#
...#..#..#
...#....##
#.#.##....
##.......#
.#..#.##.#
...##.#..#
#.##....#.

Tile 3389:
#...#.##..
#.##..#...
#.#.#.....
##...#...#
##.....#..
.#..###..#
..###.....
#.##..#.##
####....##
##.#.#####

Tile 3631:
##.###.###
#...#.....
..#.......
#....#.###
#.........
#.#.......
....##...#
........#.
#....#....
.....#.#..

Tile 2081:
##..##.#..
.##......#
........##
#.......##
......##.#
#....#....
.#.#...##.
....##..#.
#.#..#####
...#####..

Tile 1579:
##........
.....#...#
#......#..
#..#.#...#
...#..#...
#.##....##
...#......
.#...#..#.
..#....#.#
.##...#...

Tile 3019:
.##..####.
...#....##
...##.#.#.
#....#....
.........#
##..#..#.#
...#.#...#
.#.##.#.##
##..#.....
#..#..###.

Tile 2089:
##.##.#...
..#.##...#
##...#....
#........#
#..#.....#
..######.#
.......#..
...#..##..
#.#....#..
...#.#####

Tile 2297:
#.#.###...
#.##......
#..#..#...
......#..#
#....#...#
###.#..##.
#....##.##
.......#.#
.........#
.###.#.#..

Tile 1373:
#..#.####.
........#.
....#....#
.##..#...#
.#.....#.#
#.....#...
#..#..#..#
.##...##.#
##.......#
.##..#.#..

Tile 1259:
...#..##.#
....#.#..#
#..#.##.##
##..##....
#..#.#..##
#.#...#...
..#......#
...###.#.#
..........
.###..##..

Tile 2411:
.###..#.#.
.#..#..#..
#.....#..#
.#.....#..
##..#...#.
.#........
#...##....
#.#..##...
.....#.#.#
....#.##..

Tile 3359:
.##..##.##
..#.#..#.#
#.......#.
.#..###..#
.#.....#..
#.........
...#.....#
.##....#..
.........#
.#.##..#..

Tile 2437:
.#.....#..
#.#.......
....##.#..
##.###.##.
##.#....#.
#...#....#
##..##..##
...#..#..#
....##.#.#
.##.#.#...

Tile 1367:
....#####.
..#.#....#
##......#.
..##...##.
..##..##.#
..##.##..#
#.#......#
##...#...#
.....#.#..
##.###.###

Tile 3533:
##..####..
##..#....#
...#.....#
#.##..####
###..##...
....###..#
..#.....##
.##.#..#..
....#.##..
##.#.##.##

Tile 1451:
.####.#...
..##.#.#..
...#.#....
.........#
....#...##
##...##...
..#...#...
#...#.####
...#.#.###
#######...

Tile 3457:
#.##.###.#
#..#.#....
###.#....#
.#....#.##
.......##.
#...####.#
#.#####..#
#.#..#..#.
#.#.......
.#.##.....

Tile 2657:
..#####..#
......###.
#.....#..#
.#..###..#
...#.##...
#.....#..#
#....#####
....#..#..
#.#.###..#
.#..##.#..

Tile 2347:
#.##.#.###
#...#....#
..........
#.#.#.....
#....#....
#..#...###
#.##...#..
#.....#..#
#.#.###.#.
#...###.##

Tile 3923:
#.####...#
###...#.##
......#..#
..#..#..##
#....#.#.#
.........#
.....#.#.#
#..####..#
....##.#..
#..#...#..

Tile 2593:
####...#..
#...####.#
#......##.
..#..#....
#..#.....#
...#......
....#.....
#.#...##.#
.......#.#
#.#####.#.

Tile 1103:
..##...#.#
..#.....#.
#....#...#
...##....#
#.#.....##
.#.#.#.#..
#....#####
.#.#.#####
.........#
.##...#.#.

Tile 2671:
.##.##.#.#
...#.#....
##....#...
#......#..
.#....#.##
........##
..#...###.
###...#...
....#.#..#
.#######.#

Tile 3907:
###....#..
..##.....#
..........
##...#..##
##..#..##.
##..##..#.
#.#.....##
..###..#.#
#..#..#..#
#.#.......

Tile 1327:
.#..#..#.#
.###.###..
..#..#....
.#...#.#..
#.##.....#
#..#.#..#.
###...#...
##..#..#.#
......#...
#.#...###.

Tile 1163:
..#.#...#.
####.#.#..
.##.....##
#...#.....
.....####.
#...#..###
...#.##.#.
##..#..#..
.....#.#.#
.###...#.#

Tile 2683:
.......#..
.#.#.##..#
##..#...##
...#......
##.##...#.
...#.#.###
.#.##.....
#..#....#.
.###..#...
.#.#.....#

Tile 3881:
#.#.#..##.
##.#...###
##......##
#.........
#..#......
...#.#...#
.#..##....
.....##.#.
##...#....
###.....#.

Tile 2711:
#...#.#.##
#..##.....
...#......
#.#.#....#
#...#.#...
...#......
.#.#......
..#....#.#
#.##..#...
#.#.#.....

Tile 3491:
.###..#.##
.....#....
.##..#..##
##.##..#.#
.###....##
..##..#..#
..#......#
..#..#...#
#...#.#..#
.......##.

Tile 2339:
###.#####.
.#....#...
##.#.....#
#.........
#.........
..##..##.#
.###......
#...##....
##........
#.....#..#

Tile 1543:
..##...#..
.....#..##
....##....
.........#
.#..#.#..#
..........
.........#
#.#.#.....
##.#.#....
#.#.###..#

Tile 2371:
#..#.#####
##.#..###.
#.#...##..
#...###.##
#...#..#.#
##.....###
.#.......#
.....#.#..
..#.#.####
.###..##..

Tile 2971:
..###.#...
#.........
##.#...###
....###...
###......#
..###....#
##.#.....#
.#.....#..
#.....####
..##.##.#.

Tile 2203:
#.###.#.##
.#...##.#.
#.#.....#.
..#.#....#
##..#....#
##..#.....
#..#..#...
##........
..#..#.#..
##....#..#

Tile 2113:
###...##.#
......#...
.....##.#.
....####.#
##..#..#..
#...#...##
.......#.#
......#..#
#.##.#..##
....##.#..

Tile 1321:
.#..#..#..
##...#...#
..........
.####..#.#
##.#.#.#.#
#..#.#....
#..#.....#
.#.......#
....###.#.
##.#.##.##

Tile 1667:
.#####...#
##....#..#
#..#.#....
..##..#...
#..##...##
...#......
..#..#...#
.....#...#
#..#...#.#
##..#.##..

Tile 3001:
.#.#######
...#.#...#
..##...#.#
#.........
....#.....
#.....##.#
.....####.
#.........
...#.##.##
##.##..###

Tile 1789:
..###.#..#
...##...#.
........#.
####...#.#
#..#..#...
##.##..##.
....#.....
#.#.#.....
#.#..#...#
######.#..

Tile 3677:
.#...#.###
#..##...##
#..#...#.#
##........
..#....###
..........
##..#.##.#
.#........
......##..
.##.##...#

Tile 2693:
#.#.###.#.
#.#..#..#.
.#....#...
#..#...#.#
.#.##..##.
.##......#
...#.....#
##...#.###
.#.#.....#
########..

Tile 2141:
#####.....
#.........
#...#....#
....#.....
.......#..
##..#.#..#
#....#..#.
..#....#..
.....##.#.
##..#.#.#.

Tile 3847:
#...######
#..#.##..#
..#...##.#
##..#.#..#
##..#..#.#
##.##....#
#........#
#....#.#..
#.........
#......#..

Tile 2333:
#.#..##.#.
####.....#
###..#...#
.........#
...##.#..#
###.......
..##...#.#
...###...#
.###.#...#
##..#.##..

Tile 2129:
#..###.###
....#....#
#.#.#..#.#
....#....#
#...#.#..#
..##.##...
#.##....#.
#...#.#...
.#..##...#
...###.##.

Tile 1283:
.#..#.###.
###.#.##.#
#..#..#.#.
.....#...#
.#.#.#....
#...#..#.#
.#.#..#.##
.#..##..#.
#....##.#.
.#..#...#.

Tile 2851:
.##.#.....
###......#
.#........
....#.#..#
.#..#..#..
.##....#..
.##...#.##
#.###.##.#
##........
.#.#.#.#.#

Tile 2311:
#..##....#
.#......#.
....#...##
....#...#.
.#...#...#
#...#....#
.....#.#.#
.....##..#
##..##.###
##..###.#.

Tile 3217:
#.##....##
#.......#.
#.#....#.#
..##.###..
....##...#
#.........
...#.#...#
..##....##
..#......#
##.###..##

Tile 2833:
####..#..#
#...##.#.#
........##
..##..#...
.......#..
......#...
###..#..#.
.##..#...#
##.#....#.
.####.....

Tile 3593:
#..#####..
.##.#....#
...#..#..#
##.#...##.
#######...
...#..#...
#.##..#.#.
#........#
#...##..##
........##

Tile 3299:
...#.#..#.
#.#......#
##......##
.#..#..#..
#.#####...
#...#....#
#.#.......
..#....#.#
.#........
#.#.#.##..

Tile 2719:
...#..#.#.
#..#.###..
......#.##
....#.#..#
#.#......#
....#.....
##..#.#...
...##.....
###.#....#
..####..##

Tile 2161:
..#.##..##
#........#
...#.#...#
...###....
......#..#
..##.##.#.
.#....#.##
#...#####.
#....#...#
.#....#...

Tile 3643:
...#...##.
##......##
....#.###.
.#.#..####
..#.#...##
.##.#.#.#.
#.#..#..#.
#..#.#..#.
.#........
.#.###....

Tile 3767:
.##.#....#
#...#...##
.#...##..#
.#.#......
..#..#..##
#..#.#.##.
#.....#.##
....##...#
.....#.##.
..#..###..

Tile 2377:
.#####.#.#
..#.....##
#...#...#.
##....##..
....#..#..
#.##..#..#
.#...#.##.
...##...#.
#....#....
..#.#..###

Tile 2441:
....#.###.
.#..#....#
.#.###....
#...#.....
#.#.##....
....##....
....##...#
...#..##..
#...###.##
.##.#.#.#.

Tile 2273:
...#..#.#.
#..#...###
..#....#..
...###.#..
.........#
..##.#..##
###.#....#
#..#.....#
....#...#.
.##..#####

Tile 1231:
....#####.
#.....#.#.
..#....#.#
.###..##.#
#..#....##
#..#.#.#..
..#.###...
##........
..#.....##
##.##..#.#

Tile 2879:
#####.###.
#..#..#..#
...#..#..#
#.........
.#.#.#...#
...#..#..#
#.#..#.###
#.....####
.......#.#
..##.#####

Tile 2473:
.##.###..#
....##.#.#
#..#..#..#
..#.#.#...
#..##.....
.##....#..
....##..##
...#.....#
....#.....
.#####.#..

Tile 3329:
#.#..###..
......###.
.###..##..
...#.##...
#..#.#.###
#.###..##.
##..##...#
..#####..#
.#...#....
#.##.#..#.

Tile 1489:
....#....#
#.##.....#
..#......#
..#...#..#
.#....#..#
#..##...#.
..####.##.
.#..##..##
..#.....#.
##.#.##..#

Tile 3259:
..####.###
##.##..#.#
.#..#..###
#.#.......
.....#.#..
##...#.#..
.....#...#
.#....#...
..#.#....#
######....

Tile 1123:
....#.#.#.
...##.....
...##....#
#.##....##
..#...#..#
.#..#.....
.#......##
.........#
.....#...#
.##.###...

Tile 1381:
.###..#.##
#..##..#..
#.....#..#
#..#..#..#
#....#.#.#
.......#.#
#...#.#..#
##..###..#
..#..##...
..##.#..#.

Tile 2903:
#.###.###.
#..###..#.
##.......#
.#....#...
#..#..##.#
...#...#.#
#.#...##.#
......#..#
##.#..#..#
##.#..##.#

Tile 1049:
#....#.###
####.....#
#.##....#.
.#....##..
.##..#..##
#.#.#..#.#
.##...#..#
...#..#.##
.....#..##
..#.#.###.

Tile 3137:
...#.#.#.#
#.#....#..
..###...#.
..#..##..#
.#.#.##.#.
#.#..##..#
#.....#...
##...##.##
#.#...#...
.##.#.....

Tile 2131:
.#......##
...#...##.
.#.#.#.#.#
..........
...#....#.
#.#.#..#.#
..#.#....#
....##....
....##..#.
##.##..#..

Tile 3733:
.#.#....##
.#..#....#
..###.#..#
.....#...#
...#.....#
###..#....
#...#.####
......#...
..#..#....
.#####.#..

Tile 1973:
.#.#.##...
#.....#..#
#......#..
...#...#.#
..#..#...#
####.#....
....#..#.#
.#.....#.#
....#..#.#
..#..#.#..

Tile 3889:
#.##.#...#
#....##..#
.###..#.##
..#...###.
#....##...
#.##......
#.#..#..#.
...#.#...#
........##
##.#..#.#.

Tile 1129:
#...######
#...#.##.#
###.....#.
#.#..#..#.
.#....#...
..#...#..#
#....#.###
......#.#.
#.#..##.##
.###..#..#

Tile 1549:
.#..##.#..
#.#....###
...#..#...
##....#...
..##...#.#
#.#....###
..#.##.##.
.....#...#
#.##.....#
#..#.##...

Tile 2213:
#...#.#.##
#........#
...##...##
.#..##.#.#
#..##....#
...##...##
##........
....#....#
...#.#.#.#
..#.###..#

Tile 1597:
..###..#..
#......#..
.......#.#
#.#....###
#.###..#.#
##.#.##...
#.##.#..##
.#.#..##..
.##..#...#
##...#####

Tile 1777:
#..#....#.
.........#
..#.....#.
..#......#
.##....#..
#...#...#.
.#..#.....
#.....#..#
.#..##...#
#.#...#...

Tile 2503:
#..###.#..
.#..##.#..
....##....
..#..#....
#....#.#..
#.#....#.#
...#.#....
..##.#....
#.#.#....#
#.######..

Tile 3499:
.####...##
.#.##..#..
#........#
..#......#
##..#...#.
#...##....
#......#..
.#.#....##
...#..#...
.##.#...#.

Tile 1811:
#.###...##
..##...#..
..#.#..###
##.#....#.
.#...#....
##.......#
#....#...#
...##.##..
...#.....#
.####..#..

Tile 3793:
###.####..
##...###.#
...#.###..
#....#....
.....#....
...#.....#
###.####..
#.##.##.##
......#...
#.#.###...

Tile 2341:
.##..#.###
.#........
..####..#.
.#..####.#
#....#...#
......##.#
#.....#...
..........
#..###....
####.#####

Tile 3613:
..#.#.#.#.
#....###..
#......#..
#.#...#.##
#..#.....#
##.#.#.#..
...####..#
##...###.#
#.#.#....#
#..####...

Tile 3701:
..##.####.
#.##...###
.###.#....
#..#.....#
##........
###..#.###
..#...#.#.
.##...#..#
##.......#
###.#...##

Tile 1483:
#####..##.
....#..#.#
...#####.#
#.##...#..
........#.
........##
##..##.###
.###....##
...###..#.
...#.#..#.

Tile 3571:
#.#.##.##.
#......##.
#...#.#...
.#.#....#.
#.#....#..
#.#...#...
##.....#.#
##.#.#...#
#....#...#
#.#..#..##

Tile 2521:
##.##..#.#
......#..#
.#.#.#.##.
#........#
.#.##....#
.#....#.#.
##......#.
#....#....
.#..#...#.
.#..#..###

Tile 3413:
.##.##..#.
#...#..#..
##....#..#
..........
#..#...#..
##..#....#
....#.#...
#.....#...
......#.##
..##.##.#.

Tile 1933:
#..#....##
#...#..###
.#......##
......#..#
..........
##.##..###
.......###
...#...###
.#....#..#
#.#..#.#.#

Tile 1021:
.#..##.#.#
##....#...
.#........
.#.#....##
.#......##
##........
.....#..#.
#....#.#.#
#.........
..#.#.##.#

Tile 3323:
...###.#..
.#..####..
....#.....
#.....##.#
###......#
..########
.#....##.#
....##.##.
#..##....#
#.##.#..#.

Tile 1607:
....####..
##.#...#.#
.......#.#
#.........
..#..####.
..#..#.##.
.###.#...#
##...#....
#.######..
##.#.#.###

Tile 1481:
.###...#..
.#......##
.....#...#
...#...##.
#.###.##..
#......###
#.........
###...###.
.#....##..
#....###.#

Tile 1879:
###.##....
#.#.....##
#.#..#.#.#
.#...#..##
......####
#.###.....
......###.
#..#.#..##
..#......#
.#..#.....

Tile 2927:
###..##.#.
#..#....#.
##..#.....
#.#...####
#.........
..###...##
#.####...#
#.##.##..#
...#......
#....#.##.

Tile 1867:
..#..####.
##.##..#.#
.##......#
###..#....
..#......#
#..##..#.#
#.........
...#..#..#
#......#..
.#....#.#.

Tile 2647:
#####.##.#
...#...#..
#.......##
...#..#..#
..#.###...
...##.....
..#......#
.##.......
#...#.....
##...##.#.

Tile 2207:
#.#####.#.
#..#.#..##
...##....#
..#..#.#.#
#...###..#
.##.#...##
#..##..###
#.........
###.#..#.#
.#....##.#

Tile 1223:
###.....#.
#.....#.##
##.##.#...
#...##...#
#....#....
..#.##....
##....##..
..#..#....
.#.##..#..
###.#.####

Tile 1931:
##.#...###
#...##....
#..#....#.
..##..#...
#...#.....
.........#
###.##....
...##.#..#
.#.#.....#
..#..##.##

Tile 3917:
.##.#..#..
.......##.
#.##....##
...#....##
###....#.#
.##.#...##
###.......
##.....#..
....###.##
....#...#.

Tile 1663:
#.#.....##
.#.##.#..#
....#...#.
#...##...#
#..#......
.#..#.#..#
..#..#..##
...#......
#...##....
.##.######

Tile 3373:
#..###.###
##...#.#..
#..#..#..#
.#....##.#
..###..##.
...#.#...#
#.....#..#
##..#.....
.#..#.#...
.#...###.#

Tile 1319:
.#.#.#.#.#
.....#..##
..#.#.#.#.
.#.##...#.
..##......
...#......
.#........
#..#.....#
#..##....#
.#.##.#..#

Tile 2687:
#...###.#.
##.#...##.
#...#...##
#..##....#
......#.#.
.###.#.#..
##...#..##
......#..#
..........
#.....##.#

Tile 2003:
.######...
.##...###.
....#.....
#.###.#.#.
......#..#
..##.#.#.#
#.##..#.#.
####.#....
#...#.#.##
......##.#

Tile 1453:
###.#.####
...###..#.
.#..#..###
..#......#
#.#..#..##
..#..#.#..
#.#...#.#.
..##..#.#.
#..###....
..#..#..##

Tile 1871:
..#..#####
##..#....#
##.......#
#..#..#..#
#.....#.#.
#..###....
..........
.#........
.#.##.##.#
#.#..#.###

Tile 3709:
.##.#.##..
.#..##...#
.##.......
..##.#....
.........#
....#.....
###.#.##..
.#####....
#####.#...
..#..#####")

(def size 10)

(defn line->number [line]
  (loop [r line
         bit-value 1
         num 0]
    (if-let [c (first r)]
      (recur (rest r)
             (* bit-value 2)
             (if (= c \#) (+ num bit-value) num))
      num)))

(defn mirror-bits [n x]
  (loop [m 0
         in x
         out 0]

    (if (= m n)
      out
      (let [bit? (bit-and in 1)]
        (recur (inc m)
               (bit-shift-right in 1)
               (bit-or bit? (bit-shift-left out 1)))))))

(def mirror (partial mirror-bits size))

(defn borders [image-data]
  (let [lines (string/split-lines image-data)]
    ;; [north west south east]
    [(first lines)
     (apply str (mapv last lines))
     (string/reverse (last lines))
     (string/reverse (apply str (mapv first lines)))
     #_(line->number (first lines))
     #_(line->number (apply str (mapv first lines)))
     #_(mirror (line->number (last  lines)))
     #_(mirror (line->number (apply str (mapv last lines))))]))

(defn parse-tile [tile]
  (let [[read id]  (re-find #"Tile (\d+):\R" tile)
        image-data (subs tile (count read))]
    [(Long. id) (borders image-data)]))

(defn border-parts [data]
  (reduce (fn [acc [tile-num border]]
            (-> acc
               ;; rotations
               (update (nth border 0)
                       (fnil conj #{}) tile-num)
               (update (nth border 1)
                       (fnil conj #{}) tile-num)
               (update (nth border 2)
                       (fnil conj #{}) tile-num)
               (update (nth border 3)
                       (fnil conj #{}) tile-num)

               ;; mirror + rotations
               (update (string/reverse (nth border 0))
                       (fnil conj #{}) tile-num)
               (update (string/reverse (nth border 1))
                       (fnil conj #{}) tile-num)
               (update (string/reverse (nth border 2))
                       (fnil conj #{}) tile-num)
               (update (string/reverse (nth border 3))
                       (fnil conj #{}) tile-num))) {} data))

(defn parse [input]
  (let [tiles (string/split input #"\R\R")
        tile-data (into {} (map parse-tile) tiles)]
    {:data tile-data
     :border-parts (border-parts tile-data)}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PART-1

(defn rotate-right [[n e s w]]
  [w n e s])

(defn rotate-left [[n e s w]]
  [e s w n])

(defn rotate-around [[n e s w]]
  [s w n e])

(defn flip-horizontal [[n e s w]]
  [(string/reverse s) (string/reverse e) (string/reverse n) (string/reverse w)])

(defn flip-vertical [[n e s w]]
  [(string/reverse n) (string/reverse w) (string/reverse s) (string/reverse e)])

(comment
  (defn possibilities [tile]
    [(rotate-left tile)
     (rotate-right tile)
     (rotate-around tile)
     tile
     (flip-horizontal (rotate-left tile))
     (flip-horizontal  (rotate-right tile))
     (flip-horizontal (rotate-around tile))
     (flip-horizontal tile)])

  (defn fits? [requirement tile]
    (when (every? true? (map (fn [r t]
                               (if r
                                 (= r t)
                                 true))  requirement tile))
      tile))

  (defn make-match [requirement tile]
    (some (partial fits? requirement) (possibilities tile)))

  (defn matches? [requirement tile]
    (if (make-match requirement tile)
      tile))

  (defn neighbors [[x y]]
    ;; [north west south east]
    [[x (inc y)]
     [(inc x) y]
     [x (dec y)]
     [(dec x) y]])


  (defn with-requirements [reqs closed pos tile]
    (let [neighbors (neighbors pos)
          to-update (for [i (range 4)
                          :when (not (contains? closed (nth neighbors i)))]
                      [(mod (+ i 2) 4)
                       (nth neighbors i)
                       (nth tile i)])]
      (reduce (fn [acc [direction neighbor border]]
                (update acc neighbor (fnil assoc [nil nil nil nil]) direction (mirror border)))
              reqs to-update)))

  (defn build-picture [tiles]
    (when-let [start (first tiles)]
      (loop [image {[0 0] start}
             requirements (with-requirements {} #{} [0 0] start)
             to-place (rest tiles)
             closed-set #{}]
        (cond
          (empty? requirements) image
          (empty? to-place)     image
          :else             (let [[next-pos requirement] (first requirements)
                                  tile     (some (partial matches? requirement) to-place)]
                              (if tile
                                (let [matched-tile (make-match requirement tile)]
                                  (recur (assoc image next-pos matched-tile)
                                         (with-requirements requirements closed-set next-pos matched-tile)
                                         (remove #{tile} to-place)
                                         closed-set))
                                (recur image (dissoc requirements next-pos) to-place
                                       (conj closed-set next-pos))))))))

  (defn solve-1 [input]
    (let [parsed (parse input)
          ids    (keys parsed)
          tiles  (map parsed ids)]
      (build-picture tiles)))



  (def solution-1 (solve-1 raw-input)))

(def empty-picture {:used {}
                    :open {}})

(defn vec+ [a b]
  (mapv + a b))

(def new-directions (map #(mod % 4) (map #(- % 2) (range 4))))

(defn add-restrictions [open used positions tile-data]
  (let [data (map vector new-directions positions (map string/reverse tile-data))]
    (reduce (fn [acc [direction position data]]
              (if (used position)
                acc
                (update acc position
                        (fnil assoc [nil nil nil nil]) direction data))) open data)))

(defn rotate [n data]
  (case n
    0  (identity        (identity      data))
    1  (identity        (rotate-left   data))
    2  (identity        (rotate-right  data))
    3  (identity        (rotate-around data))
    4  (flip-horizontal (identity      data))
    5  (flip-horizontal (rotate-left   data))
    6  (flip-horizontal (rotate-right  data))
    7  (flip-horizontal (rotate-around data))
    8  (flip-vertical   (identity      data))
    9  (flip-vertical   (rotate-left   data))
    10 (flip-vertical   (rotate-right  data))
    11 (flip-vertical   (rotate-around data))))

(defn insert [picture data position rotation tile]
  (let [surrounding (map (partial vec+ position)
                         [[0 1] [1 0] [0 -1] [-1 0]])
        tile-data  (rotate rotation (data tile))]
    (as-> picture $
      (update $ :used assoc position [rotation tile])
      (update $ :open dissoc position)
      (update $ :open add-restrictions (:used $) surrounding tile-data))))

(defn matches?' [part data]
  (every? (fn [[x y]]
            (or (nil? x)
                (= x y))) (map vector part data)))



(defn matches? [data parts candidate]
  (let [data (data candidate)]
    (or (matches?' parts (identity        (identity      data)))
        (matches?' parts (identity        (rotate-left   data)))
        (matches?' parts (identity        (rotate-right  data)))
        (matches?' parts (identity        (rotate-around data)))
        (matches?' parts (flip-horizontal (identity      data)))
        (matches?' parts (flip-horizontal (rotate-left   data)))
        (matches?' parts (flip-horizontal (rotate-right  data)))
        (matches?' parts (flip-horizontal (rotate-around data)))
        (matches?' parts (flip-vertical   (identity      data)))
        (matches?' parts (flip-vertical   (rotate-left   data)))
        (matches?' parts (flip-vertical   (rotate-right  data)))
        (matches?' parts (flip-vertical   (rotate-around data))))))

(defn fitting-rotation [data parts candidate]
  (let [data (data candidate)]
    (reduce (fn [acc rotation]
              (when (matches?' parts (rotate rotation data))
                (reduced [rotation candidate]))) nil (range 12))))

(defn candidates [data open parts]
  (let [north      (nth open 0)
        east       (nth open 1)
        south      (nth open 2)
        west       (nth open 3)
        candidates (cond-> (set (keys data))
                     north (set/intersection (parts north))
                     east  (set/intersection (parts east))
                     south (set/intersection (parts south))
                     west  (set/intersection (parts west)))]
    (remove nil? (map (partial fitting-rotation data open) candidates))))

(defn build-picture [{data :data
                      parts :border-parts}]
  (let [start   (first (keys data))]
    (loop [picture (insert empty-picture
                           data
                           [0 0]
                           0
                           start)
           to-go   (dissoc data start)]
      (cond
        (nil? picture) :mega-ups
        (empty? to-go) (:used picture)
        (empty? (:open picture)) :upsi
        :else (let [[open-pos rotation candidate]
                    (reduce (fn [_ [open-pos open-part]]
                              (let [[[rotation candidate] & _ :as candidates]
                                    (candidates to-go open-part parts)]
                                (when (= 1 (count candidates))
                                  (reduced [open-pos rotation candidate])))) nil (:open picture))]
                (if open-pos
                  (recur (insert picture to-go open-pos rotation candidate)
                         (dissoc to-go candidate))
                  (println "XD")))))))

(defn dimensions [picture]
  (let [positions (keys picture)
        xs        (map first positions)
        ys        (map second positions)
        xmin      (apply min xs)
        xmax      (apply max xs)
        ymin      (apply min ys)
        ymax      (apply max ys)]
    [xmin xmax ymin ymax]))

(defn edges-of [picture]
  (let [[xmin xmax ymin ymax] (dimensions picture)
        edge-1    [xmin ymin]
        edge-2    [xmin ymax]
        edge-3    [xmax ymin]
        edge-4    [xmax ymax]]
    (mapv second (map picture [edge-1 edge-2 edge-3 edge-4]))))

(defn solve-1 [input]
  (let [parsed (parse input)
        picture (build-picture parsed)
        edges (edges-of picture)]
    (reduce * edges)))

;;; Part 1
(defn run-test []
  (solve-1 test-input))

(defn run-real []
  (solve-1 raw-input))

;;; Part 2
(defn interior [image-data]
  (let [lines (string/split-lines image-data)]
    (->> lines
       rest
       butlast
       (map rest)
       (map butlast)
       (mapv (partial apply str)))))

(defn draw-tile [tile]
  (let [[read id]  (re-find #"Tile (\d+):\R" tile)
        image-data (subs tile (count read))]
    [(Long. id) (interior image-data)]))

(defn draw-chunks [input]
  (let [tiles (string/split input #"\R\R")
        tile-data (into {} (map draw-tile) tiles)]
    {:data tile-data
     :length (count (second (first tile-data)))}))

(defn rotate-right' [chunk]
  (mapv (comp
         (partial apply str)
         reverse) (apply map vector chunk)))
(defn rotate-left' [chunk]
  (vec (reverse (apply map str chunk))))
(defn rotate-around' [chunk]
  (vec (reverse (map (comp
                      (partial apply str)
                      reverse) chunk))))

(defn flip-horizontal' [chunk]
  (vec (reverse chunk)))
(defn flip-vertical' [chunk]
  (mapv (comp
         (partial apply str)
         reverse) chunk))

(defn rotate-chunk [rotation draw-chunk]
  (case rotation
    0  (identity        (identity      draw-chunk))
    1  (identity        (rotate-left'   draw-chunk))
    2  (identity        (rotate-right'  draw-chunk))
    3  (identity        (rotate-around' draw-chunk))
    4  (flip-horizontal' (identity      draw-chunk))
    5  (flip-horizontal' (rotate-left'   draw-chunk))
    6  (flip-horizontal' (rotate-right'  draw-chunk))
    7  (flip-horizontal' (rotate-around' draw-chunk))
    8  (flip-vertical'   (identity      draw-chunk))
    9  (flip-vertical'   (rotate-left'   draw-chunk))
    10 (flip-vertical'   (rotate-right'  draw-chunk))
    11 (flip-vertical'   (rotate-around' draw-chunk))))

(defn draw-line [builder {:keys [length data]} picture y xmin xmax]
  (reduce (fn [builder line-num]
            (.append (reduce (fn [builder x]
                               (let [[rotation chunk] (picture [x y])
                                     draw-data (rotate-chunk rotation (data chunk))
                                     line-seg  (nth draw-data line-num)]
                                 (.append builder line-seg))) builder
                             (range xmin (inc xmax))) "\n")) builder (range length)))

(defn draw-picture [data picture]
  (let [[xmin xmax ymin ymax] (dimensions picture)
        builder (StringBuilder.)]
    (string/split-lines (.toString (reduce #(draw-line %1 data picture %2 xmin xmax)
                                           builder (range ymax (dec ymin) -1))))))


(def monster
  [#"..................#."
   #"#....##....##....###"
   #".#..#..#..#..#..#..."])

(defn find-monsters' [picture]
  (let [line-count (count picture)]
    (loop [linenum 1
           found-monsters nil]
      (if (>= linenum (dec line-count))
        found-monsters
        (let [body-matcher (re-matcher (nth monster 1) (nth picture linenum))
              found-at-current-line (loop [found-monsters []]
                                      (if (.find body-matcher)
                                        (let [start (.start body-matcher)
                                              end   (.end   body-matcher)]
                                          (if (and (re-matches (nth monster 0) (subs (nth picture (dec linenum)) start end))
                                                   (re-matches (nth monster 2) (subs (nth picture (inc linenum)) start end)))
                                            (recur (conj found-monsters [start linenum]))
                                            (recur found-monsters)))
                                        found-monsters))]
          (recur (inc linenum)
                 (into found-monsters found-at-current-line)))))))

(defn find-monsters [picture]
  (reduce (fn [_ rotation]
            (let [picture (rotate-chunk rotation picture)]
              (when-let [monsters (find-monsters' picture)]
                (reduced [rotation monsters])))) nil (range 12)))

(defn draw-head [line col]
  (str (subs line 0 (+ col 18))
       "O"
       (subs line (+ col 19) (count line))))
(defn draw-body [line col]
  (str (subs line 0 col)
       "O"
       (subs line (+ col 1) (+ col 5))
       "OO"
       (subs line (+ col 7) (+ col 11))
       "OO"
       (subs line (+ col 13) (+ col 17))
       "OOO"
       (subs line (+ col 20) (count line))))

(defn draw-feet [line col]
  (str (subs line 0 (+ col 1))
       "O"
       (subs line (+ col 2) (+ col 4))
       "O"
       (subs line (+ col 5) (+ col 7))
       "O"
       (subs line (+ col 8) (+ col 10))
       "O"
       (subs line (+ col 11) (+ col 13))
       "O"
       (subs line (+ col 14) (+ col 16))
       "O"
       (subs line (+ col 17) (count line))))

(defn draw-monster [picture [col row]]
  (-> picture
     (update (dec row) draw-head col)
     (update row       draw-body col)
     (update (inc row) draw-feet col)))

(defn draw-monsters [drawn-picture monsters]
  (reduce draw-monster drawn-picture monsters))

(defn run-2 [input]
  (let [parsed (parse input)
        picture (build-picture parsed)
        drawn-picture (draw-picture (draw-chunks input) picture)
        [rotation monsters] (find-monsters drawn-picture)
        monster-picture (draw-monsters (rotate-chunk rotation drawn-picture) monsters)]
    (println (string/join "\n" monster-picture))
    (->> monster-picture
       (apply concat)
       (filter #{\#})
       count)))

(defn run-test-2 []
  (run-2 test-input))

(defn run-real-2 []
  (run-2 raw-input))
