(ns random-stuff.day-17 
  (:require
    [clojure.string :as string]
    [clojure.set :as set]))

(def raw-input "#####..#
#..###.#
###.....
.#.#.#..
##.#..#.
######..
.##..###
###.####
")

(defn parse-line [line]
  (second (reduce (fn [[col actives] c]
                    [(inc col)
                     (if (= c \#) (conj actives col) actives)])
                  [0 #{}] line)))

(defn parse [input]
  (let [lines (string/split-lines input)]
    (into #{} (mapcat identity)
          (map-indexed (fn [col l]
                         (->> (parse-line l)
                              (map (partial vector col)))) lines))))

(defn add-dimensions [n set]
  (into #{} (map #(into % (repeat n 0)) set)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PART-1

(defn neighbors [[x y z]]
  (for [dx [-1 0 1]
        dy [-1 0 1]
        dz [-1 0 1]
        :when (not= dx dy dz 0)]
    [(+ x dx) (+ y dy) (+ z dz)]))

(def ninc (fnil inc 0))

(defn bio-zone [actives]
  (reduce (fn [acc cell]
            (update-in acc [(if (actives cell) :actives :inactives) cell] ninc))
          {:actives {} :inactives {}} (mapcat neighbors actives)))

(defn revive [inactives]
  (into nil (comp
             (filter (fn [[_ neighbors]]
                       (= 3 neighbors)))
             (map first)) inactives))

(defn survive   [actives]
  (into nil (comp
             (filter (fn [[_ neighbors]]
                       (or (= 2 neighbors)
                           (= 3 neighbors))))
             (map first)) actives))

(defn step [state]
  (let [bio-zone (bio-zone state)
        revived   (revive  (:inactives bio-zone))
        survivors (survive (:actives bio-zone))]
    (into #{} (concat revived survivors))))

(defn compute-steps [n start]
  (nth (iterate step start) n))

(defn solve-1 [input]
  (->> input
       parse
       (add-dimensions 1)
       (compute-steps 6)
       count))

(def solution-1 (solve-1 raw-input))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PART-2

(defn neighbors-2 [[x y z w]]
  (for [dx [-1 0 1]
        dy [-1 0 1]
        dz [-1 0 1]
        dw [-1 0 1]
        :when (not= dx dy dz dw 0)]
    [(+ x dx) (+ y dy) (+ z dz) (+ w dw)]))

(defn bio-zone-2 [actives]
  (reduce (fn [acc cell]
            (update-in acc [(if (actives cell) :actives :inactives) cell] ninc))
          {:actives {} :inactives {}} (mapcat neighbors-2 actives)))

(defn step-2 [state]
  (let [bio-zone (bio-zone-2 state)
        revived   (revive  (:inactives bio-zone))
        survivors (survive (:actives bio-zone))]
    (into #{} (concat revived survivors))))

(defn compute-steps-2 [n start]
  (nth (iterate step-2 start) n))

(defn solve-2 [input]
  (->> input
       parse
       (add-dimensions 2)
       (compute-steps-2 6)
       count))

(def solution-2 (solve-2 raw-input))
