(ns random-stuff.day-10 
  (:require
    [clojure.string :as string]))

(def raw-input "104
83
142
123
87
48
102
159
122
69
127
151
147
64
152
90
117
132
63
109
27
47
7
52
59
11
161
12
148
155
129
10
135
17
153
96
3
93
82
55
34
65
89
126
19
72
20
38
103
146
14
105
53
77
120
39
46
24
139
95
140
33
21
84
56
1
32
31
28
4
73
128
49
18
62
81
66
121
54
160
158
138
94
43
2
114
111
110
78
13
99
108
141
40
25
154
26
35
88
76
145")

(defn parse [input]
  (let [lines (string/split-lines input)]
    (map #(Integer. %) lines)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PART-1

(def test-input "16
10
15
5
1
11
7
19
6
12
4")



(defn solution [input]
  (let [adapters (-> input
                     parse
                     (conj 0)
                     sort
                     vec)
        highest  (peek adapters)
        adapters (conj adapters (+ highest 3))
        differences (->> adapters
                         (partition 2 1)
                         (map (fn [[x y]] (- y x)))
                         frequencies)]
    (* (get differences 3 0)
       (get differences 1 0))))

(solution raw-input)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PART-2

(def test-input2 "28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3")

(defn queue
  ([] clojure.lang.PersistentQueue/EMPTY)
  ([coll] (reduce conj clojure.lang.PersistentQueue/EMPTY coll)))

(defn solution2 [input]
  (let [adapters (-> input
                     parse
                     (conj 0)
                     sort
                     vec)
        highest  (peek adapters)
        wall     (+ highest 3)
        adapters (conj adapters wall)
        adapter-at (as-> (repeat (inc wall) 0) $
                     (vec $)
                     (reduce (fn [acc adapter]
                               (assoc acc adapter 1)) $ adapters))]
    (loop [to-go (reverse adapter-at)
           acc (queue [0 0 1])]
      (if-let [next (first to-go)]
        (let [sum  (reduce + acc)
              real (* next sum)]
          (recur (rest to-go)
                 (-> acc
                     pop
                     (conj real))))
        (last acc)))))

(solution2 raw-input)




