(ns random-stuff.day-13)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PART 1

(def raw-input [1000340
                13,nil,nil,nil,nil,nil,nil,37,nil,nil,nil,nil,nil,401,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,17,nil,nil,nil,nil,19,nil,nil,nil,23,nil,nil,nil,nil,nil,29,nil,613,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,41])

(def current-time (first raw-input))

current-time

(def busses (filter some? (rest raw-input)))

(defn bus-stops [bus]
  (map #(* % bus) (range)))

(def all-stops (map bus-stops busses))
(def all-future (map (fn [stops] (drop-while #(< % current-time) stops)) all-stops))
(def all-earliest (map first all-future))

(def bus->earliest (zipmap busses all-earliest))

(def bus-to-take (reduce (fn [current new]
                           (if (<
                                (bus->earliest new)
                                (bus->earliest current))
                             new
                             current)) busses))
(* bus-to-take (- (bus->earliest bus-to-take) current-time))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PART 2

(def input (rest raw-input))




(def transformed (->> input
                      (map-indexed (fn [idx x]
                                     (when x
                                       [(mod (- x idx) x) x])))
                      (filter some?)))



transformed

(defn calculate [[[_ start-time] & input]]
  (println start-time)
  (loop [to-do input
         available (iterate #(+ % start-time) 0)]
    (if-let [[offset time] (first to-do)]
      (recur (rest to-do)
             (filter (fn [x] (= offset (- time (mod x time)))) available))
      (first available))))


(calculate transformed)
(map #((fnil mod 1 1) 1068781 %) input)

(defn gcd [a b]
  (if (= b 0)
    a
    (recur b (mod a b))))

(defn gcd-of [l]
  (reduce gcd 0 l))

(def g (gcd-of (map second transformed)))

(def coprime (map #(update % 1 / g) transformed))

(defn calculate-2 [input]
  (let [g       (gcd-of (map second input))
        coprime (map #(update % 1 / g) transformed)]
    coprime))


transformed
(calculate-2 transformed)

(defn add-constraint [[current-x factor] [offset time]]
  (let [possible (iterate #(+ % factor) current-x)]
    [(first (filter #(= offset (mod % time)) possible)) (* factor time)]))

(first (reduce add-constraint [0 1] transformed))
