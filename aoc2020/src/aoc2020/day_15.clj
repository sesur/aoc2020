(ns random-stuff.day-15 
  (:require
    [clojure.string :as string]))

(def raw-input "9,3,1,0,8,4")

(defn parse [input]
  (as-> input $
    (string/split $ #",")
    (mapv #(Integer. %) $)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PART-1

(defn speak-number [{:keys [turn last] :as state} num]
  (if last
    (-> state
        (assoc last turn)
        (assoc :last num)
        (update :turn inc))
    (assoc state :last num)))

(defn make-state [numbers]
  (loop [l     numbers
         state {:turn 0 :last nil}]
    (if-let [x (first l)]
      (recur (rest l) (speak-number state x))
      state)))

(defn next-number [{:keys [turn last] :as state}]
  (let [spoken-at (get state last)]
    (if spoken-at
      (- turn spoken-at)
      0)))

(defn continue [state]
  (let [n (next-number state)]
    (lazy-seq (cons n (continue (speak-number state n))))))

(defn game [starting-numbers]
  (concat starting-numbers
        (continue (make-state starting-numbers))))

(def solution-1 (-> raw-input
                    parse
                    game
                    (nth (dec 2020))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PART-2

(defn speak-number! [{:keys [turn last] :as state} num]
  (if last
    (-> state
        (assoc! last turn)
        (assoc! :last num)
        (assoc!  :turn (inc turn)))
    (assoc! state :last num)))

(defn play-game! [starting-numbers n]
  (if (< n (count starting-numbers))
    (nth starting-numbers n)
    (loop [state (transient (reduce speak-number {:turn 0 :last nil} starting-numbers))]
      (if (< (:turn state) n)
        (recur (speak-number! state (next-number state)))
        (:last state)))))

(defn play-game [starting-numbers n]
  (if (< n (count starting-numbers))
    (nth starting-numbers n)
    (loop [state (reduce speak-number {:turn 0 :last nil} starting-numbers)]
      (if (< (:turn state) n)
        (recur (speak-number state (next-number state)))
        (:last state)))))

(def solution-2 (-> raw-input
                    parse
                    (play-game! (dec 30000000))))

(comment
  (def parsed (-> raw-input parse))
  (time (play-game parsed (dec 30000000)))
  (time (play-game! parsed (dec 30000000))))
