(ns aoc2020.day-22
  (:require [clojure.string :as string]))

(def test-input "Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10")

(def real-input "Player 1:
38
39
42
17
13
37
4
10
2
34
43
41
22
24
46
19
30
50
6
44
28
27
36
5
45

Player 2:
31
40
25
11
3
48
16
9
33
7
12
35
49
32
26
47
14
8
20
23
1
29
15
21
18")

(def empty-queue clojure.lang.PersistentQueue/EMPTY)

(defn parse-deck [deck-str]
  (let [lines (rest (string/split-lines deck-str))]
    (into empty-queue (map #(Integer/parseInt %) lines))))

(defn parse [input]
  (let [deck-strs (string/split input #"\n\n")]
    (mapv parse-deck deck-strs)))

(defn play [[player-1 player-2]]
  (let [card-1 (peek player-1)
        card-2 (peek player-2)]
    (if (< card-1 card-2)
      ;; player-2 wins
      [(pop player-1) (conj (pop player-2) card-2 card-1)]
      [(conj (pop player-1) card-1 card-2) (pop player-2)])))

(defn finished? [[player-1 player-2]]

  (cond
    (empty? player-1) player-2
    (empty? player-2) player-1))

(defn score [deck]
  (reduce + (map * (reverse deck) (iterate inc 1))))

(defn run [input]
  (let [decks (parse input)]
    (loop [decks decks]
      (if-let [winner (finished? decks)]
        (score winner)
        (recur (play decks))))))


;;; Part 1
(defn run-test []
  (run test-input))

(defn run-real []
  (run real-input))

;;; Part 2
(defn play-recursive [player-1 player-2]
  (loop [player-1 player-1
         player-2 player-2
         cache #{}]
    (cond
      (contains? cache [player-1 player-2]) [true player-1] ;; true == player 1 wins _game_
      (empty? player-1) [false player-2]
      (empty? player-2) [true player-1]
      :else (let [card-1 (peek player-1)
                  card-2 (peek player-2)
                  new-player1 (pop player-1)
                  new-player2 (pop player-2)]
              (cond
                (or (> card-1 (count new-player1))
                    (> card-2 (count new-player2)))
                (if (< card-1 card-2)
                  (recur new-player1
                         (conj new-player2 card-2 card-1)
                         (conj cache [player-1 player-2])) ;; player 2 wins _round_
                  (recur (conj new-player1 card-1 card-2)
                         new-player2
                         (conj cache [player-1 player-2]))) ;; player 1 wins _round_
                :else (if (first (play-recursive
                                  (into empty-queue (take card-1 new-player1))
                                  (into empty-queue (take card-2 new-player2))))
                        (recur (conj new-player1 card-1 card-2) new-player2 (conj cache [player-1 player-2]))
                        (recur new-player1 (conj new-player2 card-2 card-1) (conj cache [player-1 player-2]))))))))

(defn new-game [d1 d2]
  (vector d1 d2 #{}))

#_(defn play-recursive2 [player-1 player-2]
  (loop [games (vector (new-game player-1 player-2))
         last-winner nil]
    (if-let [[player-1 player-2 cache] (peek (:stack games))]
      (cond
        (empty? player-1) (recur (pop games) player-2)
        (empty? player-2) (recur (pop games) player-1)
        (contains? cache [player-1 player-2]) (recur (pop games) player-1)
        :else (let [card-1 (peek player-1)
                    card-2 (peek player-2)
                    new-player1 (pop player-1)
                    new-player2 (pop player-2)]
                (cond
                  (or (> card-1 (count new-player1))
                      (> card-2 (count new-player2)))
                  (if (< card-1 card-2)
                    (recur (conj (pop games) [new-player1 new-player2 (conj cache [player-1 player-2])])
                           last-winner)
                    (recur (conj (pop games) [new-player1 new-player2 (conj cache [player-1 player-2])])
                           last-winner))
                  :else (if (first (play-recursive new-player1 new-player2))
                          (recur (conj new-player1 card-1 card-2) new-player2 (conj cache [player-1 player-2]))
                          (recur new-player1 (conj new-player2 card-2 card-1) (conj cache [player-1 player-2])))))))))

(defn run-2 [input]
  (let [[player-1 player-2] (parse input)
        [_ deck] (play-recursive player-1 player-2)]
    (score deck)))

(def loop-input "Player 1:
43
19

Player 2:
2
29
14")

(defn run-test-2 []
  (run-2 test-input))

(defn run-real-2 []
  (run-2 real-input))
