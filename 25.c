#include <stdio.h>
#include <string.h>
#define int long
#ifdef TEST
int CardPublic = 5764801;
int DoorPublic = 17807724;
#else
int CardPublic = 6930903;
int DoorPublic = 19716708;
#endif

int Transform(int LoopSize, int Subject)
{
	int Value = 1;
	for (int I = 0;
	     I < LoopSize;
	     ++I)
	{
		Value *= Subject;
		Value %= 20201227;
	}

	return Value;
}

int main(int argc, char *argv[], char *envp[])
{
#ifdef PART1

        int FoundCardSecret = 0;
	int Value = 1;
	int Secret;

	for (Secret = 1;;
	     ++Secret)
	{
		Value *= 7;
		Value %= 20201227;
                if (Value == CardPublic)
		{
                        FoundCardSecret = 1;
                        goto found;
		} else if (Value == DoorPublic) {
                        goto found;
		}
	}
found:

	int EncryptionKey = Transform(Secret, FoundCardSecret ? DoorPublic : CardPublic);

	printf("Card? %d, Secret: %d, Key: %d\n", FoundCardSecret, Secret, EncryptionKey);
#else

#endif


	return 0;
}
