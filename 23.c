#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#ifdef TEST
long Input[] = {3, 8, 9, 1, 2, 5, 4, 6, 7};
#else
long Input[] = {6, 8, 5, 9, 7, 4, 2, 1, 3};
#endif

struct cup
{
	long Label;
	struct cup *Next;
};

struct cup *FindSmallest(long NumCups, struct cup *Position[], struct cup *Current, struct cup *PickupStart)
{
	// we assume here that NumCups is also the maximum labe

	long Label = Current->Label;
	long Offsets[3] = {1, 2, 3};

	for (long Idx = 0;
	     Idx < 3;
	     ++Idx)
	{
                long Tmp = Label - PickupStart->Label;
		if (Tmp < 1) Tmp += NumCups;
		if (1 <= Tmp && Tmp <= 3) {
			Offsets[Tmp-1] = -1;
		}
		PickupStart = PickupStart->Next;
	}
	long Offset = 4;
	for (long Idx = 0;
	     Idx < 3;
	     ++Idx)
	{
		if (Offsets[Idx] != -1)
		{
			Offset = Offsets[Idx];
			break;
		}
	}
	long SmallestLabel = Label - Offset;
	if (SmallestLabel < 1) SmallestLabel += NumCups;


	struct cup *Smallest = Position[SmallestLabel-1];

	return Smallest;
}

void PrlongResult(struct cup *Position[])
{
        struct cup *One = Position[0];
	for (struct cup *Cup = One->Next;
             Cup != One;
	     Cup = Cup->Next)
	{
		printf("%d", Cup->Label);
	}
	puts("");
}

int main(long argc, char *argv[], char *envp[])
{
#ifdef PART1
	long NumCups   = sizeof(Input) / sizeof(*Input);
	long NumMoves  = 100;
#else
	long NumCups   = 1000000;
	long NumMoves  = 10000000;
#endif
	struct cup *Cups = malloc(sizeof(*Cups) * NumCups);
	for (long Idx = 0;
	     Idx < NumCups;
	     ++Idx)
	{
		if (Idx < sizeof(Input) / sizeof(*Input))
		{
			Cups[Idx].Label = Input[Idx];
		} else {
			Cups[Idx].Label = Idx + 1;
		}
		if (Idx < NumCups-1)
		{
			Cups[Idx].Next = &Cups[Idx+1];
		} else {
			Cups[Idx].Next = &Cups[0];
		}


	}
	struct cup **Position = malloc(sizeof(*Position) * NumCups);
	for (long Idx = 0;
	     Idx < NumCups;
	     ++Idx)
	{
		Position[Cups[Idx].Label - 1] = &Cups[Idx];
	}

        struct cup *Current = &Cups[0];

	for (long Move = 0;
	     Move < NumMoves;
	     ++Move)
	{
		struct cup *PickupStart = Current->Next;
		struct cup *PickupEnd = Current->Next->Next->Next;

		Current->Next = PickupEnd->Next;

		struct cup *Destination = FindSmallest(NumCups, Position, Current, PickupStart);
                PickupEnd->Next = Destination->Next;
                Destination->Next = PickupStart;

		Current = Current->Next;
	}
#ifdef PART1
	PrintResult(Position);
#else
	struct cup *One = Position[0];
	long N = One->Next->Label;
	long NN = One->Next->Next->Label;
	printf("Result: %ld * %ld = %ld\n", N, NN, N * NN);
#endif
	return 0;
}
