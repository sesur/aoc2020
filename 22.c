#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef TEST
int Player1[] = {
	9, 2, 6, 3, 1
};

int Player2[] = {
	5, 8, 4, 7, 10
};

#else
int Player1[] = {
	38,
	39,
	42,
	17,
	13,
	37,
	 4,
	10,
	 2,
	34,
	43,
	41,
	22,
	24,
	46,
	19,
	30,
	50,
	 6,
	44,
	28,
	27,
	36,
	 5,
	45
};


int Player2[] = {
	31,
	40,
	25,
	11,
	 3,
	48,
	16,
	 9,
	33,
	 7,
	12,
	35,
	49,
	32,
	26,
	47,
	14,
	 8,
	20,
	23,
	 1,
	29,
	15,
	21,
	18
};

#endif

typedef struct
{
	int Start, End, Count;
	int BufferSize;
	int *Buffer;
} ring_buffer;

ring_buffer Init(int Size)
{
	ring_buffer Buf;
	Buf.Start = 0;
	Buf.End = 0; // one past end
	Buf.Count = 0;
	Buf.BufferSize = Size;
	Buf.Buffer = malloc(sizeof(int) * Size);
	return Buf;
}

int Pop(ring_buffer *Buf)
{
//	printf("Pop: %d %d %d\n", Buf->Start, Buf->End, Buf->BufferSize);
	assert(Buf->Count != 0);
	int C = Buf->Buffer[Buf->Start];
	Buf->Start += 1;
	if (Buf->Start == Buf->BufferSize) Buf->Start = 0;
	Buf->Count -= 1;
	return C;
}

void Queue(ring_buffer *Buf, int C)
{
//	printf("Queue: %d %d %d\n", Buf->Start, Buf->End, Buf->BufferSize);
	assert(Buf->Count != Buf->BufferSize);
	Buf->Buffer[Buf->End] = C;
        Buf->End += 1;
	if (Buf->End == Buf->BufferSize) Buf->End = 0;
	Buf->Count += 1;
}

int Score(ring_buffer *Buf)
{
        int Result = 0;

        for (int I = Buf->Count;
	     I > 0;
	     --I)
	{
		Result += I * Pop(Buf);
	}
	return Result;
}

void Print(ring_buffer *Buf)
{
#ifdef DEBUG

	for (int I = 0;
	     I < Buf->BufferSize;
	     ++I)
	{
		if (I != 0) {
			printf(", ");
		}
		if (I == Buf->Start) printf("|>");
		printf("%d", Buf->Buffer[I]);
		if (I == Buf->End) printf("<|");
	}
#else
	int Current = Buf->Start;
	for (int I = 0;
	     I < Buf->Count;
	     ++I)
	{
		if (I != 0) {
			printf(", ");
		}

		printf("%d", Buf->Buffer[Current]);
		Current += 1;
		if (Current == Buf->BufferSize) Current = 0;
	}
#endif
}

#ifndef PART1

ring_buffer Copy(ring_buffer *Buf)
{
	ring_buffer C;
	C.Start = Buf->Start;
	C.End = Buf->End; // one past end
	C.Count = Buf->count;
	C.BufferSize = Buf->Size;
	C.Buffer = malloc(sizeof(int) * Buf->Size);
	if (Buf->Start < Buf->End)
	{
		memcpy(C.Buffer + Buf->Start, Buf->Buffer + Buf->Start,
		       sizeof(int) * (Buf->End - Buf->Start + 1));
	} else {
		memcpy(C.Buffer + Buf->Start, Buf->Buffer + Buf->Start,
		       sizeof(int) * (Buf->Count - Buf->End));
		memcpy(C.Buffer, Buf->Buffer, sizeof(int) * Buf->End);
	}
	return C;
}

void Free(ring_buffer *Buf)
{
	free(Buf->Buffer);
}

// returns wether Deck1 won
int Game(ring_buffer *Deck1, ring_buffer *Deck2)
{
	int Has1Won;
	while (Deck1->Count > 0 && Deck2->Count > 0)
	{
		int Card1 = Pop(Deck1);
		int Card2 = Pop(Deck2);
		int HasEnough1 = Card1 <= Deck1->Count;
		int HasEnough2 = Card2 <= Deck2->Count;
		if (HasEnough1 && !HasEnough2)
		{
			Has1Won = 1;
		} else if (!HasEnough1 && HasEnough2)
		{
			Has1Won = 0;
		} else if (!HasEnough1 && !HasEnough2)
		{
			Has1Won = Card1 > Card2;
		} else {
			ring_buffer Copy1 = Copy(Deck1);
			ring_buffer Copy2 = Copy(Deck2);

			Has1Won = Game(Copy1, Copy2);
		}

		if (Has1Won) {
			Queue(Deck1, Card1);
			Queue(Deck1, Card2);
		} else {
			Queue(Deck2, Card2);
			Queue(Deck2, Card1);
		}
	}

	// the winner of the last round is also the winner of the game
	return Has1Won;
}

struct round_list;
typedef struct round_list round_list;

struct round_list
{
        int *Round;
	round_list *Next;
};

typedef struct
{
	int CardCount;
	ring_buffer Deck1;
	ring_buffer Deck2;
	round_list  Entry;
	round_list  *End;
} game;

game NewGame(ring_buffer *Deck1, ring_buffer *Deck2)
{
	game G;
	G.CardCount   = Deck1->Count + Deck2->Count;
	G.Deck1       = Init(CardCount);
	G.Deck2       = Init(CardCount);
	G.Entry.Round = NULL;
	G.Entry.Next  = NULL;
	G.End         = &G.Entry;
}

void FreeGame(game *Game)
{
	round_list *Current = Entry->Next;
	if (Current)
	{
		round_list *Next = Current->Next;
		free(Current->Round);
		free(Current);
	}
}

struct game_stack;
typedef struct game_stack game_stack;

struct game_stack
{
	game *Game;
	game_stack *Front;
};

game_stack Push(game_stack *Front, game *Game)
{
	game_stack New;
	New.Front = Front;
	New.Game  = Game;
	return New;
}

game *Peek(game_stack *Front)
{
        game *Result = Front.Game;

	return Result;
}

void NextRound(game_stack *Stack)
{

}

#endif

int main(int argc, char *argv[], char *envp[])
{
	int Player1CardCount = sizeof(Player1) / sizeof(*Player1);
	int Player2CardCount = sizeof(Player2) / sizeof(*Player2);
	int CardCount = Player1CardCount + Player2CardCount;


	ring_buffer Deck1 = Init(CardCount);
	for (int I = 0;
	     I < Player1CardCount;
	     ++I)
	{
		Queue(&Deck1, Player1[I]);
	}

	ring_buffer Deck2 = Init(CardCount);
	for (int I = 0;
	     I < Player2CardCount;
	     ++I)
	{
		Queue(&Deck2, Player2[I]);
	}

	int Round = 1;
	while (Deck1.Count > 0 && Deck2.Count > 0)
	{
#ifdef TEST
		printf("-- Round %d --\n", Round);
		printf("Player 1's deck: ");
		Print(&Deck1);
		puts("");
		printf("Player 2's deck: ");
		Print(&Deck2);
		puts("");
#endif
		int Card1 = Pop(&Deck1);
		int Card2 = Pop(&Deck2);
#ifdef TEST
		printf("Player 1 plays %d\n", Card1);
		printf("Player 2 plays %d\n", Card2);
#endif

		// all cards are assumed to be unique
		if (Card1 > Card2)
		{
#ifdef TEST
			printf("Player 1 wins the round\n");
#endif
                        Queue(&Deck1, Card1);
			Queue(&Deck1, Card2);
		} else {
#ifdef TEST
			printf("Player 2 wins the round\n");
#endif
                        Queue(&Deck2, Card2);
			Queue(&Deck2, Card1);
		}
		Round++;
#ifdef TEST
		puts("");
#endif
	}
#ifdef TEST
	printf("== Post-game results ==\n");
	printf("-- Round %d --\n", Round);
	printf("Player 1's deck: ");
	Print(&Deck1);
	puts("");
	printf("Player 2's deck: ");
	Print(&Deck2);
	puts("");
	puts("");
#endif

	if (Deck1.Count > 0)
	{
		printf("Result: %d\n", Score(&Deck1));
	} else {
		printf("Result: %d\n", Score(&Deck2));
	}

	return 0;
}
